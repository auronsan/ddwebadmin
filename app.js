var express = require('express'),
path = require('path'),
logger = require('morgan'),
cookieParser = require('cookie-parser'),
bodyParser = require('body-parser'),
cors = require('cors');
pg = require('pg');
//conString = "postgres://postgres:eatingisfortheweak@10.1.0.5/helloDoctor";
//prod
/*
ERP_DB_HOST = 52.220.215.50
ERP_DB_PORT = 5432
ERP_DB_USER = drdrhelp
ERP_DB_PASS = doctorssaymeanth12gs
ERP_DB_NAME = helloDoctor
*/
conString = "postgres://drdrhelp:doctorssaymeanth12gs@52.220.215.50/helloDoctor";
//conString = "postgres://postgres:eatingisfortheweak@10.1.0.16/helloDoctor";
database = new pg.Client(conString);
database.connect();

require('dotenv').config();
const port = process.env.PORT || 3002;router = express.Router();

app = express();

app.listen(port, () => console.log(`Listening on port ${port}`));

var index = require('./routes/index');
var users = require('./routes/users');

var doctors = require('./routes/doctors');
var patients = require('./routes/patients');


var business = require('./routes/business');

var payments = require('./routes/payments');
var appointment = require('./routes/appointment');


var ddcard = require('./routes/ddcard');

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');

app.use(cors());
// uncomment after placing your favicon in /public
//app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));
app.use('/', index);

app.use('/api/', index);

app.use('/api/doctors', doctors);

app.use('/api/patients', patients);

app.use('/api/appointment', appointment);

app.use('/api/users', users);

app.use('/api/business', business);

app.use('/api/payments', payments);

app.use('/api/ddcard',ddcard)


// catch 404 and forward to error handler
app.use(function(req, res, next) {
   
  
  var err = new Error('Not Found');
  err.status = 404;
  next(err);
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
   
  
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

module.exports = app;
