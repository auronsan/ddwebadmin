import React from "react";
import Cookies from 'universal-cookie';

import { NavLink } from "react-router-dom";
const cookies = new Cookies();

const Chart = window.Chart;
class Navbar extends React.Component {
  componentDidMount() {
   /*if (cookies.get('tokenHrforte')) {
      window.location.href = "#home";
    } else {
      window.location.href = "/";
    }*/
  }
  render() {
return (
<nav className="navbar navbar-expand-lg navbar-dark bg-dark fixed-top" id="mainNav">
<NavLink to="/home"  className="navbar-brand">Admin Doctor.help</NavLink>
      
    <button className="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
      <span className="navbar-toggler-icon"></span>
    </button>
    <div className="collapse navbar-collapse" id="navbarResponsive">
      <ul className="navbar-nav navbar-sidenav" id="exampleAccordion">
        <li className="nav-item" data-toggle="tooltip" data-placement="right" title="Dashboard">
        <NavLink to="/home"  className="nav-link"> <i className="fa fa-fw fa-dashboard"></i>
            <span className="nav-link-text">Dashboard</span></NavLink>
       
         
        </li>
        <li className="nav-item" data-toggle="tooltip" data-placement="right" title="Tables">
        <a className="nav-link nav-link-collapse collapsed" data-toggle="collapse" href="#collapseUsers" data-parent="#exampleAccordion">
            <i className="fa fa-fw fa-table"></i>
            <span className="nav-link-text">Table</span>
          </a>
          <ul className="sidenav-second-level collapse" id="collapseUsers">
           
            <li>
            <NavLink to="/Getusers"  className="nav-link"><i className="fa fa-fw fa-table"></i>
            <span className="nav-link-text">All Users Table</span></NavLink>
            </li>

            <li>
            <NavLink to="/Appointments"  className="nav-link"><i className="fa fa-fw fa-table"></i>
            <span className="nav-link-text">All Appointments</span></NavLink>
            </li>
            <li>
            <NavLink to="/AppointmentsCall"  className="nav-link"><i className="fa fa-fw fa-table"></i>
            <span className="nav-link-text">All Appointments Call</span></NavLink>
            </li>
            <li>
            <NavLink to="/AppointmentsMedicalRecord"  className="nav-link"><i className="fa fa-fw fa-table"></i>
            <span className="nav-link-text">All Appointments Medical Record</span></NavLink>
            </li>
            <li>
            <NavLink to="/AppointmentsReview"  className="nav-link"><i className="fa fa-fw fa-table"></i>
            <span className="nav-link-text">All Appointments Review</span></NavLink>
            </li>
            <li>
            <NavLink to="/AppointmentsTransaction"  className="nav-link"><i className="fa fa-fw fa-table"></i>
            <span className="nav-link-text">All Appointments Transaction</span></NavLink>
            </li>

            <li>
            <NavLink to="/BusinessTransfer"  className="nav-link"><i className="fa fa-fw fa-table"></i>
            <span className="nav-link-text">Business Transfer</span></NavLink>
            </li>

            <li>
            <NavLink to="/DoctorProfile"  className="nav-link"><i className="fa fa-fw fa-table"></i>
            <span className="nav-link-text">Doctor Profile</span></NavLink>
            </li>
            <li>
            <NavLink to="/DoctorLanguages"  className="nav-link"><i className="fa fa-fw fa-table"></i>
            <span className="nav-link-text">Doctor Language</span></NavLink>
            </li>
            <li>
            <NavLink to="/DoctorSpecialties"  className="nav-link"><i className="fa fa-fw fa-table"></i>
            <span className="nav-link-text">Doctor Specialties</span></NavLink>

            <NavLink to="/DoctorLicense"  className="nav-link"><i className="fa fa-fw fa-table"></i>
            <span className="nav-link-text">Doctor License</span></NavLink>
            </li>
            <li>
            <NavLink to="/GeneralMedicalRecord"  className="nav-link"><i className="fa fa-fw fa-table"></i>
            <span className="nav-link-text">General Medical Record</span></NavLink>
            </li>
            <li>
            <NavLink to="/Promotion"  className="nav-link"><i className="fa fa-fw fa-table"></i>
            <span className="nav-link-text">Promotion</span></NavLink>
            </li>
            <li>
            <NavLink to="/UserWallet"  className="nav-link"><i className="fa fa-fw fa-table"></i>
            <span className="nav-link-text">User Wallet</span></NavLink>
            </li>
          </ul>
        </li>
        <li className="nav-item" data-toggle="payment" data-placement="right" title="Payments">
          <a className="nav-link nav-link-collapse collapsed" data-toggle="collapse" href="#collapsePayments" data-parent="#exampleAccordion">
            <i className="fa fa-fw fa-credit-card"></i>
            <span className="nav-link-text">Payments</span>
          </a>
          <ul className="sidenav-second-level collapse" id="collapsePayments">
            <li>
            <NavLink to="/Payments"   target="_self"  className="nav-link"><i className="fa fa-fw fa-credit-card"></i>
            <span className="nav-link-text">All Payment Data</span></NavLink>
            </li>
            <li> <NavLink to={"/Payments?gate=stripe"}   target="_self" className="nav-link"><i className="fa fa-fw fa-credit-card"></i>
            <span className="nav-link-text">Stripe Payment Data</span></NavLink>
            </li>
            <li> <NavLink to={"/Payments?gate=onepay"}   target="_self" className="nav-link"><i className="fa fa-fw fa-credit-card"></i>
            <span className="nav-link-text">Onepay Payment Data</span></NavLink>
            </li>
            <li> <NavLink to={"/Payments?gate=drdr"}   target="_self" className="nav-link"><i className="fa fa-fw fa-credit-card"></i>
            <span className="nav-link-text">DrDr Payment Data</span></NavLink>
            </li>
            

          </ul>
        </li>
        <li className="nav-item" data-toggle="ddcardmanagement" data-placement="right" title="ddcardmanagement">
          <a className="nav-link nav-link-collapse collapsed" data-toggle="collapse" href="#collapseddcardmanagement" data-parent="#exampleAccordion">
            <i className="fa fa-fw fa-credit-card"></i>
            <span className="nav-link-text">ddcardmanagement</span>
          </a>
          <ul className="sidenav-second-level collapse" id="collapseddcardmanagement">
            <li>
            <NavLink to={"/ddcardmanagement"}   target="_self" className="nav-link"><i className="fa fa-fw fa-credit-card"></i>
            <span className="nav-link-text">DD Card List</span></NavLink>
 </li>
            <li> <NavLink to={"/"}   target="_self" className="nav-link"><i className="fa fa-fw fa-credit-card"></i>
            <span className="nav-link-text">DD Card Active</span></NavLink>
            </li>
            <li> <NavLink to={"/"}   target="_self" className="nav-link"><i className="fa fa-fw fa-credit-card"></i>
            <span className="nav-link-text">DD Card Generated</span></NavLink>
            </li>
            <li> <NavLink to={"/"}   target="_self" className="nav-link"><i className="fa fa-fw fa-credit-card"></i>
            <span className="nav-link-text">DrDr Payment Data</span></NavLink>
            </li>
            

          </ul>
        </li>
        <li>
      
            </li>
      
     </ul>
      <ul className="navbar-nav ml-auto">
        <li className="nav-item dropdown">
          <a className="nav-link dropdown-toggle mr-lg-2" id="messagesDropdown" href="#" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            <i className="fa fa-fw fa-envelope"></i>
            <span className="d-lg-none">Messages
              <span className="badge badge-pill badge-primary">12 New</span>
            </span>
            <span className="indicator text-primary d-none d-lg-block">
              <i className="fa fa-fw fa-circle"></i>
            </span>
          </a>
          <div className="dropdown-menu" aria-labelledby="messagesDropdown">
            <h6 className="dropdown-header">New Messages:</h6>
            <div className="dropdown-divider"></div>
            <a className="dropdown-item" href="#">
              <strong>David Miller</strong>
              <span className="small float-right text-muted">11:21 AM</span>
              <div className="dropdown-message small">Hey there! This new version of SB Admin is pretty awesome! These messages clip off when they reach the end of the box so they don't overflow over to the sides!</div>
            </a>
            <div className="dropdown-divider"></div>
            <a className="dropdown-item" href="#">
              <strong>Jane Smith</strong>
              <span className="small float-right text-muted">11:21 AM</span>
              <div className="dropdown-message small">I was wondering if you could meet for an appointment at 3:00 instead of 4:00. Thanks!</div>
            </a>
            <div className="dropdown-divider"></div>
            <a className="dropdown-item" href="#">
              <strong>John Doe</strong>
              <span className="small float-right text-muted">11:21 AM</span>
              <div className="dropdown-message small">I've sent the final files over to you for review. When you're able to sign off of them let me know and we can discuss distribution.</div>
            </a>
            <div className="dropdown-divider"></div>
            <a className="dropdown-item small" href="#">View all messages</a>
          </div>
        </li>
        <li className="nav-item dropdown">
          <a className="nav-link dropdown-toggle mr-lg-2" id="alertsDropdown" href="#" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            <i className="fa fa-fw fa-bell"></i>
            <span className="d-lg-none">Alerts
              <span className="badge badge-pill badge-warning">6 New</span>
            </span>
            <span className="indicator text-warning d-none d-lg-block">
              <i className="fa fa-fw fa-circle"></i>
            </span>
          </a>
          <div className="dropdown-menu" aria-labelledby="alertsDropdown">
            <h6 className="dropdown-header">New Alerts:</h6>
            <div className="dropdown-divider"></div>
            <a className="dropdown-item" href="#">
              <span className="text-success">
                <strong>
                  <i className="fa fa-long-arrow-up fa-fw"></i>Status Update</strong>
              </span>
              <span className="small float-right text-muted">11:21 AM</span>
              <div className="dropdown-message small">This is an automated server response message. All systems are online.</div>
            </a>
            <div className="dropdown-divider"></div>
            <a className="dropdown-item" href="#">
              <span className="text-danger">
                <strong>
                  <i className="fa fa-long-arrow-down fa-fw"></i>Status Update</strong>
              </span>
              <span className="small float-right text-muted">11:21 AM</span>
              <div className="dropdown-message small">This is an automated server response message. All systems are online.</div>
            </a>
            <div className="dropdown-divider"></div>
            <a className="dropdown-item" href="#">
              <span className="text-success">
                <strong>
                  <i className="fa fa-long-arrow-up fa-fw"></i>Status Update</strong>
              </span>
              <span className="small float-right text-muted">11:21 AM</span>
              <div className="dropdown-message small">This is an automated server response message. All systems are online.</div>
            </a>
            <div className="dropdown-divider"></div>
            <a className="dropdown-item small" href="#">View all alerts</a>
          </div>
        </li>
      
        <li className="nav-item">
          <a className="nav-link" data-toggle="modal" data-target="#exampleModal">
            <i className="fa fa-fw fa-sign-out"></i>Logout</a>
        </li>
      </ul>
    </div>
    
  </nav>
  
)
  }

  
}


export default Navbar;