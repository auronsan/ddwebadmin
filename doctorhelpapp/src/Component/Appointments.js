import React from "react";
import { NavLink } from "react-router-dom";
import axios from "axios";
const getQuery = window.getQuery;

//import $ from "jquery";
const $ = window.$;
const editable = window.editable;


const swal = window.swal;
class Appointments extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      isLogged: false,
      AppointmentData: [],
      idDoctor: 0,
      count: null,
    }
  }

  componentDidMount() {

    var url = process.env.REACT_APP_API_URL +"api/appointment/all";
    var idUser = getQuery("id");
    if (idUser) {
      url = process.env.REACT_APP_API_URL +"api/appointment?id=" + idUser;
    }
    swal({
       title: 'Loading..',         html: '<div class="inner"><span class="spinner"></span><img src="img/logo-call.png"/></div>',  showConfirmButton:false,          
        allowOutsideClick: false,
    }
    );
    axios.get(url)
      .then((s) => {
        var response = s.data;
        console.log(response);
        if (response.status == "success") {
          var data = response.result;
          var length = data.length;
          this.setState({ AppointmentData: data, count: length });
        
          swal.close();
        } else {
          swal.close();
          swal(
            'Error!',
            response.status,
            'error'
          )
        }
      })
      .catch((error) => {
        swal.close();
        console.log(error);
        swal(
          'Error!',
          error.toString(),
          'error'
        )
      });

  }

  render() {
    return (
     
        <div className="content-wrapper">
        
          <div className="card-header">
            <i className="fa fa-table"></i> Data Appointments</div>
          <div className="card-body">
            <div className="table-responsive">
              <table className="table table-bordered" id="dataTable" width="100%" cellSpacing="0">
                <thead className="card-header">
                  <tr>
                    <th>id</th>
                    <th>created_date</th>
                    <th>schedule_date	</th>
                    <th>type	</th>
                    <th>state	</th>
                    <th>note	</th>
                    <th>doctor_id	</th>
                    <th>patient_id	</th>
                    <th>rating_point	</th>
                    <th>rating_comment	</th>
                    <th>cost	</th>
                    <th>unit_cost	</th>
                    <th>review_update	</th>
                     <th>write_date	</th>
<th>consultation_note</th>	
<th>prom_trans_id	</th>
<th>consultation_duration</th>	
<th>parent_id	</th>
<th>reschedule_count</th>	
<th>consultation_fee	</th>
<th>suspend_reason	</th>
<th>is_accepted</th>
                  </tr>
                </thead>
                <tbody>
                  {this.state.count == 0 && <tr ><td colSpan="31" style={{textAlign: "center"}}>Not Found</td></tr>}
                  {this.state.AppointmentData.map((item, i) => {
                   

                    return <tr key={i} id={"doctor_" + item.id}>
                      <td ><div>{item.id}</div></td>
                      <td ><div>{item.created_date}</div></td>                   
                      <td ><div>{item.schedule_date}	</div></td>
                      <td ><div>{item.type}	</div></td>
                      <td ><div>{item.state}	</div></td>
                      <td ><div>{item.note}	</div></td>
                      <td ><div>{item.doctor_id}	</div></td>
                      <td ><div>{item.patient_id}	</div></td>
                      <td ><div>{item.rating_point}	</div></td>
                      <td ><div>{item.rating_comment}	</div></td>
                      <td ><div>{item.cost}	</div></td>
                      <td ><div>{item.unit_cost}	</div></td>
                      <td ><div>{item.review_update}	</div></td>
                      <td ><div>{item.write_date}	</div></td>
                      <td ><div>{item.consultation_note}	</div></td>
                      <td ><div>{item.prom_trans_id}	</div></td>
                      <td ><div>{item.consultation_duration}	</div></td>
<td ><div>{item.parent_id}	</div></td>
<td ><div>{item.reschedule_count}	</div></td>
<td ><div>{item.consultation_fee}	</div></td>
<td ><div>{item.suspend_reason}	</div></td>
<td ><div>{item.is_accepted}</div></td>
                    </tr>
                  })}

                </tbody>
              </table>
            </div>
          </div>
          <div className="card-footer small text-muted"></div>
        </div>
    );
  }
}

export default Appointments;