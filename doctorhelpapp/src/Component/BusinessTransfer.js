import React from "react";
import { NavLink } from "react-router-dom";

import axios from "axios";
const getQuery = window.getQuery;


//import $ from "jquery";
const $ = window.$;
const editable = window.editable;
const select2 = window.select2;

const swal = window.swal;
class BusinessTransfer extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      isLogged: false,
      BusinessData: [],
      idDoctor: 0,
      count: null,
    }
  }
  componentDidMount() {

    var url = process.env.REACT_APP_API_URL +"api/business";
    var id = getQuery("id");
    if (id) {
      url = process.env.REACT_APP_API_URL +"api/business?id=" + id;
    }
    swal({
      title: 'Loading..', html: '<div class="inner"><span class="spinner"></span><img src="img/logo-call.png"/></div>', showConfirmButton: false,

      allowOutsideClick: false,
    }
    );
    axios.get(url)
      .then((s) => {
        var response = s.data;
        console.log(response);
        if (response.status == "success") {
          var data = response.result;
          var length = data.length;
          this.setState({ BusinessData: data, count: length });

          swal.close();
        } else {
          swal.close();
          swal(
            'Error!',
            response.status,
            'error'
          )
        }
      })
      .catch((error) => {
        swal.close();
        console.log(error);
        swal(
          'Error!',
          error.toString(),
          'error'
        )
      });

  }
  render() {
    return (
      <div className="content-wrapper">

        <div className="card-header">
          <i className="fa fa-table"></i> Data Business Transfer </div>
        <div className="card-body">
          <div className="table-responsive">
            <table className="table table-bordered" id="dataTable" width="100%" cellSpacing="0">
              <thead className="card-header">
                <tr>
                  <th>id</th>
                  <th>type</th>

                  <th>amount</th>
                  <th>trans_date</th>
                  <th>state	</th>
                  <th>note</th>
                  <th>business_type</th>
                  <th>payment_transaction_id</th>
                  <th>withdraw_transaction_id		</th>
                  <th>created_date</th>
                  <th>write_date	</th>
                  <th>wallet_source_id</th>

                  <th>wallet_dest_id</th>

                  <th>appointment_id</th>
                  <th>fee_type	</th>
                  <th>currency</th>
                  <th>balance_source_before</th>
                  <th>balance_dest_before</th>
                  <th>balance_source_after</th>
                  <th>balance_dest_after</th>
                  <th>amount_commission</th>
                  <th>amount_commission1</th>
                </tr>

              </thead>
              <tbody>
                {this.state.count == 0 && <tr ><td colSpan="9" style={{ textAlign: "center" }}>No Result</td></tr>}
                {this.state.BusinessData.map((item, i) => {


                  return <tr key={i} id={"doctor_" + item.id}>
                    <td ><div>{item.id}</div></td>
                    <td ><div>{item.type}	</div></td>
                    <td ><div>{item.amount}	</div></td>
                    <td ><div>{item.trans_date}</div></td>
                    <td ><div>{item.state}	</div></td>
                    <td ><div>{item.note}	</div></td>
                    <td ><div>{item.business_type}	</div></td><td >
                      <div>{item.payment_transaction_id}	</div></td>
                    <td ><div>{item.withdraw_transaction_id}	</div></td>
                    <td ><div>{item.created_date}</div></td>
                    <td ><div>{item.write_date}	</div></td>
                    <td ><div>{item.wallet_source_id}	</div></td>
                    <td ><div>{item.wallet_dest_id}</div></td>
                    <td ><div>{item.appointment_id}	</div></td>
                    <td ><div>{item.fee_type}	</div></td>
                    <td ><div>{item.currency}	</div></td><td >
                      <div>{item.balance_source_befored}	</div></td>
                    <td ><div>{item.balance_dest_before}	</div></td>
                    <td >    <div>{item.balance_source_after}</div></td>
                    <td ><div>{item.balance_dest_after}	</div></td>
                    <td ><div>{item.amount_commission}	</div></td>
                    <td ><div>{item.amount_commission1}</div></td>
                  </tr>
                })}

              </tbody>
            </table>
          </div>
        </div>
        <div className="card-footer small text-muted"></div>
      </div>
    );
  }
}

export default BusinessTransfer;