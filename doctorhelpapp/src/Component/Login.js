
import React from "react";
import Cookies from 'universal-cookie';
import $ from 'jquery';

import { NavLink } from "react-router-dom";

import axios from "axios";
const getQuery = window.getQuery;
const swal = window.swal;
const cookies = new Cookies();

class Login extends React.Component {

  onLogin(e) {
    
    var email = $('#exampleInputEmail1').val();
    var password = $('#exampleInputPassword1').val();
    var urlLogin = process.env.REACT_APP_API_URL +"api/login";
    
    axios.post(urlLogin,{
      email: email,
      password: password
    }).then((s) => {
            var response = s.data;
            console.log(response);
            if (response.status == "success") {
                this.props.setLogged();
            } else {
                swal(
                    'Error!',
                    response.message,
                    'error'
                )
            }
        })
        .catch((error) => {
            swal(
                'Error!',
                error.toString(),
                'error'
            )
        });

  }
  componentDidMount() {
    
  }
  render() {
    return (
      <div className="container">
    <div className="card card-login mx-auto mt-5">
      <div className="card-header">Login</div>
      <div className="card-body">
        <form>
          <div className="form-group">
            <label for="exampleInputEmail1">Email address</label>
            <input className="form-control" id="exampleInputEmail1" type="email" aria-describedby="emailHelp" placeholder="Enter email"/>
          </div>
          <div className="form-group">
            <label for="exampleInputPassword1">Password</label>
            <input className="form-control" id="exampleInputPassword1" type="password" placeholder="Password"/>
          </div>
          <div className="form-group">
            <div className="form-check">
              <label className="form-check-label">
                <input className="form-check-input" type="checkbox"/> Remember Password</label>
            </div>
          </div>
          <a onClick={this.onLogin.bind(this)}  className="btn btn-primary btn-block">Login</a>
        </form>
        <div className="text-center">
          <a className="d-block small mt-3" href="register.html">Register an Account</a>
          <a className="d-block small" href="forgot-password.html">Forgot Password?</a>
        </div>
      </div>
    </div>
  </div>
    );
  }
}


export default Login;
