import React from "react";
import { NavLink } from "react-router-dom";


import axios from "axios";
const getQuery = window.getQuery

//import $ from "jquery";
const $ = window.$;
const editable = window.editable;
const select2 = window.select2;

const swal = window.swal;
class GeneralMedicalRecord extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            isLogged: false,
            PatientData: [],
            idDoctor: 0,
            count: null,
        }
    }
    componentDidMount() {
        var url = process.env.REACT_APP_API_URL +"api/patients/generalMedical";


        swal({
            title: 'Loading..', html: '<div class="inner"><span class="spinner"></span><img src="img/logo-call.png"/></div>', showConfirmButton: false,

            allowOutsideClick: false,
        }
        );
        axios.get(url)
            .then((s) => {
                var response = s.data;
                console.log(response);
                if (response.status == "success") {
                    var data = response.result;
                    var length = data.length;
                    this.setState({ PatientData: data, count: length });
                    swal.close();
                } else {
                    swal.close();
                    swal(
                        'Error!',
                        response.status,
                        'error'
                    )
                }
            })
            .catch((error) => {
                swal.close();
                console.log(error);
                swal(
                    'Error!',
                    error.toString(),
                    'error'
                )
            });

    }

    refreshData() {
        var url = process.env.REACT_APP_API_URL +"api/users";


        swal({
            title: 'Loading..', html: '<div class="inner"><span class="spinner"></span><img src="img/logo-call.png"/></div>', showConfirmButton: false,

            allowOutsideClick: false,
        }
        );
        axios.get(url)
            .then((s) => {
                var response = s.data;
                console.log(response);
                if (response.status == "success") {
                    var data = response.result;
                    var length = data.length;
                    this.setState({ PatientData: data, count: length });

                    swal.close();
                } else {
                    swal.close();
                    swal(
                        'Error!',
                        response.status,
                        'error'
                    )
                }
            })
            .catch((error) => {
                swal.close();
                console.log(error);
                swal(
                    'Error!',
                    error.toString(),
                    'error'
                )
            });

    }
    render() {
        return (

            <div className="content-wrapper" >


                <div className="card-header">
                    <i className="fa fa-table"></i> General Medical Record</div>
                <div className="card-body" >
                    <div className="table-responsive">
                        <table className="table table-bordered" id="dataTable" width="100%" cellSpacing="0">
                            <thead className="card-header">
                                <tr>
                                    <th>    id	</th>
                                    <th> create_date	</th>
                                    <th> write_date	</th>
                                    <th> user_mngment_id	</th>
                                    <th> height	</th>
                                    <th> weight	</th>
                                    <th> blood_type	</th>
                                    <th> smoking	</th>
                                    <th> alcohol_intake	</th>
                                    <th> sleep_rest	</th>
                                    <th> exercise	</th>
                                    <th> stress_coping	</th>
                                    <th> roles_relationship	</th>
                                    <th> psychococial	</th>
                                    <th> other_substance</th>
                                </tr>

                            </thead>
                            <tbody>
                                {this.state.count == 0 && <tr ><td colSpan="15" style={{ textAlign: "center" }}>Not Found</td></tr>}
                                {this.state.PatientData.map((item, i) => {


                                    return <tr key={i} id={"doctor_" + item.id}>

                                      <td ><div>  {item.id}	</div></td>
                                        <td ><div>{item.create_date}</div></td>
                                         <td ><div> {item.write_date}</div></td>
                                         <td ><div> {item.user_mngment_id}</div></td>
                                         <td ><div> {item.height}</div></td>
                                         <td ><div> {item.weight}</div></td>
                                         <td ><div> {item.blood_type}</div></td>
                                         <td ><div> {item.smoking}</div></td>
                                         <td ><div> {item.alcohol_intake}</div></td>
                                         <td ><div> {item.sleep_rest}</div></td>
                                         <td ><div> {item.exercise}</div></td>
                                         <td ><div>{item.stress_coping}</div></td>
                                         <td ><div>{item.roles_relationship}</div></td>
                                         <td ><div> {item.psychococial}</div></td>
                                         <td ><div> {item.other_substance}</div></td>
                                      </tr>
                                })}

                            </tbody>
                        </table>
                    </div>
                </div>
                <div className="card-footer small text-muted"></div>
            </div>
        );
    }
}

export default GeneralMedicalRecord;