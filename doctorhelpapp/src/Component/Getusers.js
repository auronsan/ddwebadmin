import React from "react";
import { NavLink } from "react-router-dom";


import axios from "axios";
const getQuery = window.getQuery

//import $ from "jquery";
const $ = window.$;
const editable = window.editable;

const swal = window.swal;

class Getusers extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      isLogged: false,
      UserData: [],
      idDoctor: 0,
      count: null,
    }
  }
  componentDidMount() {
    var url = process.env.REACT_APP_API_URL +"api/users";

    console.log(url);
    swal({
      title: 'Loading..', html: '<div class="inner"><span class="spinner"></span><img src="img/logo-call.png"/></div>', showConfirmButton: false,
      showConfirmButton: false,
      allowOutsideClick: false,
    }
    );
    axios.get(url)
      .then((s) => {
        var response = s.data;
        console.log(response);
        if (response.status == "success") {
          var data = response.result;
          var length = data.length;
          this.setState({ UserData: data, count: length });
          this.callEditable();
          swal.close();
        } else {
          swal.close();
          swal(
            'Error!',
            response.status,
            'error'
          )
        }
      })
      .catch((error) => {
        swal.close();
        console.log(error);
        swal(
          'Error!',
          error.toString(),
          'error'
        )
      });

  }

  search(e) {
    e.preventDefault();

    swal({
      title: 'Loading..', html: '<div class="inner"><span class="spinner"></span><img src="img/logo-call.png"/></div>', showConfirmButton: false,
      showConfirmButton: false,
      allowOutsideClick: false,
    }
    );

    var url = process.env.REACT_APP_API_URL +"api/users/search?search=" + $("#query").val();

    axios.get(url)
      .then((s) => {
        var response = s.data;
        console.log(response);
        if (response.status == "success") {
          var data = response.result;
          var length = data.length;
          this.setState({ UserData: data, count: length });
          $(".editDoctor").editable("destroy");
          $('.editDoctorBool').editable("destroy");
          $('.editDoctorValid').editable("destroy");
          $('.editDoctorRole').editable("destroy");
          this.callEditable();
          swal.close();
        } else {
          swal.close();
          swal(
            'Error!',
            response.status,
            'error'
          )
        }
      })
      .catch((error) => {
        swal.close();
        console.log(error);
        swal(
          'Error!',
          error.toString(),
          'error'
        )
      });
  }
  callEditable() {
    $('.editDoctor').editable({
      url: process.env.REACT_APP_API_URL +'api/users/',
      ajaxOptions: {
        type: 'PUT',
      },

      params: (params) => {
        console.log(params);
        return params;
      },
      success: (s) => {

        console.log(s);
      },
      error: (e) => {
        console.log(e);
      }
    });

    $('.editDoctorBool').editable({
      url: process.env.REACT_APP_API_URL +'api/users/',
      ajaxOptions: {
        type: 'PUT',
      },
      source: "[{value: '1', text: 'true'}, {value: '0', text: 'false'}]",
      params: (params) => {
        console.log(params);
        if (params.value == 1) {
          params.value = true;
        } else {
          params.value = false;
        }
        return params;
      },
      success: (s) => {

        console.log(s);
      },
      error: (e) => {
        console.log(e);
      }
    });
    $('.editDoctorValid').editable({
      url: process.env.REACT_APP_API_URL +'api/users/',
      ajaxOptions: {
        type: 'PUT',

      },
      source: "[{value:'active',text:'active'},{value:'inactive',text:'inactive'}]",
      params: (params) => {
        console.log(params);

        return params;
      },
      success: (s) => {

        console.log(s);
      },
      error: (e) => {
        console.log(e);
      }
    });
    $('.editDoctorRole').editable({
      url: process.env.REACT_APP_API_URL +'api/users/role',
      ajaxOptions: {
        type: 'PUT',

      },
      source: "[{value:'doctor',text:'doctor'},{value:'patient',text:'patient'}]",
      params: (params) => {

        return params;
      },
      success: (s) => {

        console.log(s);
      },
      error: (e) => {
        console.log(e);
      }
    });

  }

  changeStatus(idDoc) {
    var valDoc = $("#validation_" + idDoc).text();
    console.log(valDoc);
    var BoolAct = "inactive";
    if (valDoc == "active") {
      BoolAct = "inactive";
    } else {
      BoolAct = "active";
    }
    swal({
      title: "Do you want to " + BoolAct + " ?",
      text: "",
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes'
    }).then((result) => {

      if (result.value == true) {
        $.ajax({
          url: process.env.REACT_APP_API_URL +'api/users',
          type: 'put',
          data: {
            pk: idDoc,
            name: "validation_status",
            value: BoolAct
          },
          success: (s) => {

            if (valDoc == "active") {
              $("#validation_" + idDoc).text("inactive");
            } else {
              $("#validation_" + idDoc).text("active");
            }

          }, error: (e) => {
            console.log(e);
          }
        });
      }
    })
  }
  validateEmail(email) {
    var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(email);
  }
  addUser() {
    var steps = [
      {
        title: 'Email',
        input: 'email',
        showLoaderOnConfirm: true,
        preConfirm: (email) => {
          return new Promise((resolve) => {
            setTimeout(() => {
              this.state.UserData.map((item, i) => {
                if (item.email == email) {
                  swal.showValidationError(
                    'This email is already taken.'
                  )
                }
              })
              resolve();
            }, 2000)
          })
        }
      },
      {
        title: 'Phone Number',
        input: 'number',
      },
      {
        title: 'Password',
        input: 'password',
      },
      {
        title: 'Role ',
        input: 'select',
        inputOptions: { 'doctor': 'doctor', 'patient': 'patient' }
      },
    ]
    swal.setDefaults({
      input: 'text',
      confirmButtonText: 'Next &rarr;',
      showCancelButton: true,
      progressSteps: ['1', '2', '3', '4']
    });
    swal.queue(steps).then((result) => {
      swal.resetDefaults()

      if (result.value) {
        axios.post('http://doctordoctor.help:8000/wd/user/register ', {
          email: result.value[0],
          phoneNumber: result.value[1],
          password: result.value[2],
          userRoleCode: result.value[3]
        })
          .then((response) => {
            console.log(response);
            this.refreshData();
          })
          .catch(function (error) {
            console.log(error);
          });
      }
    })
  }

  deleteAll() {
    var checkbox = $("#dataTable input:checkbox:checked");
    var isExist = false;
    var deletedId = [];
    console.log(checkbox);
    $.each(checkbox, (index, value) => {
      var singleId = (($(value).attr("id")).split("_"))[2];
      deletedId.push(singleId);
      isExist = true;
    });
    console.log(deletedId);
    if (isExist) {
      swal({
        title: 'Are you sure?',
        text: "You won't be able to revert this!",
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Yes, delete it!'
      }).then((result) => {
        if (result.value) {
          axios.delete(process.env.REACT_APP_API_URL +'api/users', {
            data: { id: deletedId }
          })
            .then((response) => {
              console.log(response);
              $.each(deletedId, (index, value) => {
                $("#doctor_" + value).fadeOut();
              });

            })
            .catch(function (error) {
              console.log(error);
            });
        }
      });
      isExist = false;
    } else {

    }
  }

  refreshData() {
    var url = process.env.REACT_APP_API_URL +"api/users";


    swal({
      title: 'Loading..', html: '<div class="inner"><span class="spinner"></span><img src="img/logo-call.png"/></div>', showConfirmButton: false,

      allowOutsideClick: false,
    }
    );
    axios.get(url)
      .then((s) => {
        var response = s.data;
        console.log(response);
        if (response.status == "success") {
          var data = response.result;
          var length = data.length;
          this.setState({ UserData: data, count: length });
          this.callEditable();
          swal.close();
        } else {
          swal.close();
          swal(
            'Error!',
            response.status,
            'error'
          )
        }
      })
      .catch((error) => {
        swal.close();
        console.log(error);
        swal(
          'Error!',
          error.toString(),
          'error'
        )
      });

  }
  render() {
    return (

      <div className="content-wrapper" >
        <form className="form-inline" style={{ "padding": "10px" }} onSubmit={this.search.bind(this)}>
          <div className="input-group">
            <input className="form-control" type="text" name="query" id="query" placeholder="Search User with Email Or Phone" />
            <span className="input-group-btn">
              <button className="btn btn-primary" type="button">
                <i className="fa fa-search"></i>
              </button>
            </span>
          </div>
        </form>
        <button className="btn btn-success" style={{ margin: "10px" }} onClick={this.addUser.bind(this)}><i className="fa fa-plus"></i>Add</button>
        <button className="btn btn-danger" style={{ margin: "10px" }} onClick={this.deleteAll.bind(this)}><i className="fa fa-minus"></i>Delete</button>

        <div className="card-header">
          <i className="fa fa-table"></i> Data Users</div>
        <div className="card-body" >
          <div className="table-responsive" id="redips-drag">
            <table className="table table-bordered" id="dataTable" width="100%" cellSpacing="0">
              <thead className="card-header">
                <tr>

                  <th></th>
                  <th>id</th>
                  <th>created_date</th>
                  <th>Email</th>
                  <th>Email Confirmed</th>
                  <th>Password</th>
                  <th>Phone</th>
                  <th>Phone_confirmed</th>
                  <th>user_mngment_id</th>
                  <th>validation_status</th>
                  <th>user_role</th>
                  <th>Appointment</th>
                </tr>
              </thead>
              <tbody>
                {this.state.count == 0 && <tr ><td colSpan="9" style={{ textAlign: "center" }}>Not Found</td></tr>}
                {this.state.UserData.map((item, i) => {
                  let phoneConfirmation = 0;
                  if (item.phone_confirmed == true) {
                    phoneConfirmation = 1;
                  }
                  let emailConfirmation = 0;
                  if (item.email_confirmed == true) {
                    emailConfirmation = 1;
                  }
                  let validationStatus = false;
                  if (item.validation_status == "active") {
                    validationStatus = true;
                  }

                  return <tr key={i} id={"doctor_" + item.id}>
                    <td> <input type="checkbox" className="filled-in" id={"doctor_check_" + item.id} /> <label htmlFor={"doctor_check_" + item.id}></label></td>

                    <td ><div className="redips-drag">{item.id}</div></td>
                    <td ><div className="editDoctor" data-type="date" data-name="created_date" data-pk={item.id} data-title="Enter new value">{item.created_date}</div></td>
                    <td ><div className="editDoctor" data-type="text" data-name="email" data-pk={item.id} data-title="Enter new value">{item.email}</div></td>
                    <td ><div className="editDoctorBool" data-type="select" data-name="email_confirmed" data-pk={item.id} data-value={emailConfirmation} data-title="Enter new value">{item.email_confirmed.toString()}</div></td>
                    <td ><div className="editDoctor" data-type="text" data-name="password" data-pk={item.id} data-title="Enter new value">{item.password}</div></td>
                    <td ><div className="editDoctor" data-type="text" data-name="phone" data-pk={item.id} data-title="Enter new value">{item.phone}</div></td>
                    <td><div className="editDoctorBool" data-type="select" data-value={phoneConfirmation} data-name="phone_confirmation" data-pk={item.id} data-title="Enter new value">{item.phone_confirmed.toString()}</div></td>
                    <td><div className="editDoctor" data-type="number" data-name="user_mngment_id" data-pk={item.id} data-title="Enter new value">{item.user_mngment_id}</div></td>
                    {validationStatus && <td><div className="validDoctor" id={"validation_" + item.id} onClick={this.changeStatus.bind(this, item.id)}>active</div></td>}
                    {!validationStatus && <td><div className="validDoctor" id={"validation_" + item.id} onClick={this.changeStatus.bind(this, item.id)}>inactive</div></td>}
                    <td><div className="editDoctorRole" data-type="select" data-name="code" data-pk={item.id} data-title="Enter new value">{item.code}</div></td>
                    <td><div>   <NavLink to={"/Appointments?id=" + item.id} className="nav-link"><i className="fa fa-fw fa-calendar"></i>View
         </NavLink></div></td>

                  </tr>
                })}

              </tbody>
            </table>
          </div>
        </div>
        <div className="card-footer small text-muted"> Click to edit data</div>
      </div>
    );
  }
}

export default Getusers;