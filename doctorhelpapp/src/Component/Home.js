import React from "react";
import Cookies from 'universal-cookie';
 
import { NavLink } from "react-router-dom";

import axios from "axios";
const cookies = new Cookies();

const getQuery = window.getQuery;


axios.defaults.timeout = 10000;

//import $ from "jquery";
const $ = window.$;
const swal = window.swal;
const Chart = window.Chart;
class Home extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
        isLogged: false,
        callLogsData: {
          result: {},
        },
        callLogsMonth: {
          result: {},
        },
        stripeLogs: {
          result: {},
        },
        drdrLogs: {
          result: {},
        },
        idDoctor: 0,
        count: null,
    }
    console.log(props);
}
  componentWillReceiveProps() {
   
    //this.FetchData();

}
FetchStripe(){
  var url = process.env.REACT_APP_API_URL + 'api/getstripetransaction';
  axios.get(url)
  .then((s) => {
      var response = s.data;
      if (response.status == "success") {
          var data = response.result;
          var lengthObj = window.objLength(data);
          console.log('test');
          console.log(lengthObj);
          if(lengthObj > 0 ) {
            this.setState({ stripeLogs: data});
          }
      } else {
          /*swal(
              'Error!',
              response.status,
              'error'
          )*/
      }
  })
  .catch((error) => {
      console.log(error);
      swal(
          'Error!',
          error.toString(),
          'error'
      )
  });
}

FetchDrDr(){
  var url = process.env.REACT_APP_API_URL + 'api/getddtransaction';
  axios.get(url)
  .then((s) => {
      var response = s.data;
      if (response.status == "success") {
          var data = response.result;
          var lengthObj = window.objLength(data);
          if(lengthObj > 0 ) {
            this.setState({ drdrLogs: data});
          }
      } else {
          /*swal(
              'Error!',
              response.status,
              'error'
          )*/
      }
  })
  .catch((error) => {
      console.log(error);
      swal(
          'Error!',
          error.toString(),
          'error'
      )
  });
}
FetchCallMonth() {
    var url = process.env.REACT_APP_API_URL + 'api/getcallmonth';
    axios.get(url)
    .then((s) => {
        var response = s.data;
        if (response.status == "success") {
            var data = response.result;
            var lengthObj = window.objLength(data);
            if(lengthObj > 0 ) {
              this.setState({ callLogsMonth: data});
            }
        } else {
            /*swal(
                'Error!',
                response.status,
                'error'
            )*/
        }
    })
    .catch((error) => {
        console.log(error);
        swal(
            'Error!',
            error.toString(),
            'error'
        )
    });
}
FetchData() {
  this.FetchStripe();
  this.FetchDrDr();
  this.FetchCallMonth();
  var url = process.env.REACT_APP_API_URL +"api/getcalltoday";
    swal({
        title: 'Loading..', html: '<div class="inner"><span class="spinner"></span><img src="img/logo-call.png"/></div>', showConfirmButton: false,
        allowOutsideClick: false,
    }
    );
    
    axios.get(url)
        .then((s) => {
            var response = s.data;
            if (response.status == "success") {
                var data = response.result;
                var length = 0;
                var lengthObj = window.objLength(data);
                if(lengthObj > 0 ) {
                  this.setState({ callLogsData: data});
                }
                swal.close();
            } else {
                swal.close();
                /*swal(
                    'Error!',
                    response.status,
                    'error'
                )*/
            }
        })
        .catch((error) => {
            swal.close();
            console.log(error);
            swal(
                'Error!',
                error.toString(),
                'error'
            )
        });
}
  componentDidMount() {
   this.FetchData();  /*if (cookies.get('tokenHrforte')) {
      window.location.href = "#home";
    } else {
      window.location.href = "/";
    }*/
     this.createChart();
  }
  createChart() {

   
    Chart.defaults.global.defaultFontFamily='-apple-system,system-ui,BlinkMacSystemFont,"Segoe UI",Roboto,"Helvetica Neue",Arial,sans-serif',
    Chart.defaults.global.defaultFontColor="#292b2c";
    
    var datacall =  new Array(30);
    
    if(this.state.callLogsMonth.length > 0 ) {
      this.state.callLogsMonth.map( (e,v) => {
        datacall[e.timeday] = e.count;
      })
    }
    
    
    console.log(datacall);
    var dateMonth = [];

    for(var i = 1 ; i < 31; i ++) {
      dateMonth.push(i);
    }
    
    var barChartData = {
			labels: dateMonth,
			datasets: [{
				label: 'Call',
        data: datacall,
            backgroundColor: [
                'rgba(255, 99, 132, 0.2)',
                'rgba(54, 162, 235, 0.2)',
                'rgba(255, 206, 86, 0.2)',
                'rgba(75, 192, 192, 0.2)',
                'rgba(153, 102, 255, 0.2)',
                'rgba(255, 159, 64, 0.2)'
            ],
            borderColor: [
                'rgba(255,99,132,1)',
                'rgba(54, 162, 235, 1)',
                'rgba(255, 206, 86, 1)',
                'rgba(75, 192, 192, 1)',
                'rgba(153, 102, 255, 1)',
                'rgba(255, 159, 64, 1)'
            ],
            borderWidth: 1
			}]
		};
    var ctx=document.getElementById("myAreaChart"),
    myLineChart=new Chart(ctx,{type:"line",data:barChartData,
    options:{scales:{xAxes:[{time:{unit:"date"},gridLines:{display:!1},ticks:{maxTicksLimit:7}}],
    yAxes:[{labelString: 'Date of the Month'}]
  
  },legend:{display:!1}}});
      
  }
  render() {
    console.log(this.state.stripeLogs.length);
    return (
  <div className="content-wrapper">
    <div className="container-fluid" style={{height:"700px",maxHeight:"100%"}}>
      <ol className="breadcrumb">
        <li className="breadcrumb-item">
          <a href="#">Dashboard</a>
        </li>
        <li className="breadcrumb-item active">My Dashboard</li>
      </ol>
      <div className="row">
        <div className="col-xl-3 col-sm-6 mb-3">
          <div className="card text-white bg-primary o-hidden h-100">
            <div className="card-body">
              <div className="card-body-icon">
                <i className="fa fa-fw fa-phone"></i>
              </div>
              <div className="mr-5">{(this.state.callLogsData.length > 0 && this.state.callLogsData.length) ? this.state.callLogsData.length : 0 } Call Today</div>
            </div>
            <NavLink to={"/calldetailtoday"} class="card-footer text-white clearfix small z-1">
              <span className="float-left">View Details</span>
              <span className="float-right">
                <i className="fa fa-angle-right"></i>
              </span>
            </NavLink>
          </div>
        </div>
        <div className="col-xl-3 col-sm-6 mb-3">
          <div className="card text-white bg-warning o-hidden h-100">
            <div className="card-body">
              <div className="card-body-icon">
                <i className="fa fa-fw fa-cc-stripe"></i>
              </div>
              <div className="mr-5">{(this.state.stripeLogs.length > 0 && this.state.stripeLogs.length) ? this.state.stripeLogs.length : 0 } Stripe Transaction today</div>
            </div>
            <NavLink to={"/stripedetailtoday"} class="card-footer text-white clearfix small z-1">
              <span className="float-left">View Details</span>
              <span className="float-right">
                <i className="fa fa-angle-right"></i>
              </span>
              </NavLink>
          </div>
        </div>
        <div className="col-xl-3 col-sm-6 mb-3">
          <div className="card text-white bg-success o-hidden h-100">
            <div className="card-body">
              <div className="card-body-icon">
                <i className="fa fa-fw fa-credit-card"></i>
              </div>
              <div className="mr-5">{(this.state.drdrLogs.length > 0 && this.state.drdrLogs.length) ? this.state.drdrLogs.length : 0 } DD Card Transaction today</div>
            </div>
            <NavLink to={"/drdrdetailtoday"} class="card-footer text-white clearfix small z-1">
              <span className="float-left">View Details</span>
              <span className="float-right">
                <i className="fa fa-angle-right"></i>
              </span>
              </NavLink>
          </div>
        </div>
      </div>
      <div className="card mb-3">
        <div className="card-header">
          <i className="fa fa-area-chart"></i> Total Call / day on this Month</div>
        <div className="card-body">
          <canvas id="myAreaChart" width="100%" height="30"></canvas>
        </div>
        <div className="card-footer small text-muted"></div>
      </div>
      
    </div>
  
    <footer className="sticky-footer">
      <div className="container">
        <div className="text-center">
          <small>Copyright © dd admin 2018</small>
        </div>
      </div>
    </footer>
  
    <a className="scroll-to-top rounded" href="#page-top">
      <i className="fa fa-angle-up"></i>
    </a>
   
  
    </div>
    );
  }
}


export default Home;