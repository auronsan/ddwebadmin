import React from "react";
import { NavLink } from "react-router-dom";

 

//import $ from "jquery";
const $ = window.$;
const editable = window.editable;
const select2 = window.select2;

const swal = window.swal;
class AppointmentsReview extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      isLogged: false,
      CallHistoryData: [],
      idDoctor: 0,
      count: null,
    }
  }
  componentDidMount() {
    swal({
       title: 'Loading..',         html: '<div class="inner"><span class="spinner"></span><img src="img/logo-call.png"/></div>',  showConfirmButton:false,          
      
      allowOutsideClick: false,
  }
  );
    $.ajax({
      url: process.env.REACT_APP_API_URL +'api/appointment/review',
      type: 'GET',

      success: (s) => {
        var response = JSON.parse(s);
        if (response.status == "success") {
          var data = response.result;
          var length = data.length;
          this.setState({ CallHistoryData: data, count: length });
          this.callEditable();
          swal.close();
        } else {
          swal(
            'Error!',
            response.status,
            'error'
          )
        }


      }
    });

  }

  search(e) {
    e.preventDefault();
    $.ajax({
      url: process.env.REACT_APP_API_URL +'api/doctors/search',
      type: 'GET',
      data: {
        search: $("#query").val(),
      },
      success: (s) => {
        var response = JSON.parse(s);
        if (response.status == "success") {
          var data = response.result;
          var count = data.length;
          this.setState({ CallHistoryData: data ,countDoctor: count});
          this.callEditable();
          swal.close();
        } else {
          swal(
            'Error!',
            response.status,
            'error'
          )
        }
      }, error: (e) => {
        console.log(e);
      }
    });
  }
  callEditable() {
    $('.editDoctor').editable({
      url: process.env.REACT_APP_API_URL +'api/users/',
      type: 'post',

      params: (params) => {
        console.log(params);
        return params;
      },
      success: (s) => {

        console.log(s);
      },
      error: (e) => {
        console.log(e);
      }
    });

    $('.editDoctorBool').editable({
      url: process.env.REACT_APP_API_URL +'api/users/',
      type: 'post',
      source: "[{value: '1', text: 'true'}, {value: '0', text: 'false'}]",
      params: (params) => {
        console.log(params);
        if (params.value == 1) {
          params.value = true;
        } else {
          params.value = false;
        }
        return params;
      },
      success: (s) => {

        console.log(s);
      },
      error: (e) => {
        console.log(e);
      }
    });

    $('.editDoctorValid').editable({
      url: process.env.REACT_APP_API_URL +'api/users/',
      type: 'post',
      source: "[{value:'active',text:'active'},{value:'inactive',text:'inactive'}]",
      params: (params) => {
        console.log(params);

        return params;
      },
      success: (s) => {

        console.log(s);
      },
      error: (e) => {
        console.log(e);
      }
    });

  }
  changeStatus(idDoc) {
    var valDoc = $("#validation_" + idDoc).text();
    var BoolAct = "inactive";
    if (valDoc == "active") {
      BoolAct ="inactive";
    } else {
      BoolAct ="active";
    }
    swal({
      title: "Do you want to " + BoolAct  + " ?",
      text: "",
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes'
    }).then((result) => {
      $.ajax({
        url: process.env.REACT_APP_API_URL +'api/users',
        type: 'POST',
        data: {
          pk: idDoc,
          name: "validation_status",
          value: valDoc
        },
        success: (s) => {
          if (valDoc == "active") {
            $("#validation_" + idDoc).text("inactive");
          } else {
            $("#validation_" + idDoc).text("active");
          }

        }, error: (e) => {
          console.log(e);
        }
      });
    })
  }
  render() {
    return (
        <div className="content-wrapper">
        
          <div className="card-header">
            <i className="fa fa-table"></i> Data Review</div>
          <div className="card-body">
            <div className="table-responsive">
              <table className="table table-bordered" id="dataTable" width="100%" cellSpacing="0">
                <thead className="card-header">
                  <tr>
                    <th>id</th>
               
                    <th>appointment_id</th>
                    <th>comment		</th>
                    <th>rating_point	</th>
                    <th>create_date		</th>
                     <th>write_date	</th>
                  </tr>	


                </thead>
                <tbody>
                  {this.state.count == 0 && <tr ><td colSpan="9" style={{textAlign: "center"}}>No Result</td></tr>}
                  {this.state.CallHistoryData.map((item, i) => {
                   

                    return <tr key={i} id={"doctor_" + item.id}>
                      <td ><div>{item.id}</div></td>
                      <td ><div>{item.appointment_id}</div></td>                   
                      <td ><div>{item.comment	}	</div></td>
                      <td ><div>{item.rating_point}	</div></td>
                      <td ><div>{item.create_date}	</div></td>
                      <td ><div>{item.write_date}	</div></td>
                     
                    </tr>
                  })}

                </tbody>
              </table>
            </div>
          </div>
          <div className="card-footer small text-muted"></div>
        </div>
    );
  }
}

export default AppointmentsReview;