import React from "react";
import { NavLink } from "react-router-dom";

import axios from "axios";
const getQuery = window.getQuery;

axios.defaults.timeout = 10000;


//import $ from "jquery";
const $ = window.$;
const editable = window.editable;
const select2 = window.select2;

const swal = window.swal;

const Materialize = window.Materialize;
class Payments extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            isLogged: false,
            PaymentData: [],
            idDoctor: 0,
            count: null,
        }
        console.log(props);
    }

    componentWillReceiveProps() {
        this.FetchData();

    }
    FetchData() {

        var url = process.env.REACT_APP_API_URL +"api/payments";

        var idGate = getQuery("gate");
        if (idGate) {
            url = process.env.REACT_APP_API_URL +"api/payments?gate=" + idGate;
        }
        swal({
            title: 'Loading..', html: '<div class="inner"><span class="spinner"></span><img src="img/logo-call.png"/></div>', showConfirmButton: false,
            allowOutsideClick: false,
        }
        );
        axios.get(url)
            .then((s) => {
                var response = s.data;
                console.log(response);
                if (response.status == "success") {
                    var data = response.result;
                    var length = data.length;
                    this.setState({ PaymentData: data, count: length });

                    swal.close();
                } else {
                    swal.close();
                    swal(
                        'Error!',
                        response.status,
                        'error'
                    )
                }
            })
            .catch((error) => {
                swal.close();
                console.log(error);
                swal(
                    'Error!',
                    error.toString(),
                    'error'
                )
            });
    }
    componentDidMount() {
        this.FetchData();

    }

    render() {
        return (
            <div className="content-wrapper">

                <div className="card-header" >
                    <i className="fa fa-table"></i>Payment Transaction</div>
                <div className="card-body">
                    <div className="table-responsive">
                        <table className="table table-bordered" id="dataTable" width="100%" cellSpacing="0">
                            <thead >
                                <tr >
                                    <th>id</th>
                                    <th>merref	</th>
                                    <th>amount		</th>
                                    <th>phone_card_sn	</th>
                                    <th>trans_date		</th>
                                    <th>state	</th>
                                    <th>note		</th>
                                    <th>created_date	</th>
                                    <th>write_date	</th>
                                    <th>type	</th>
                                    <th>payment_gateway	</th>
                                    <th>user_wallet_id	</th>
                                    <th>gateway_trans_code	</th>
                                    <th>customer_token	</th>
                                    <th>user_payment_info_id</th>
                                </tr>


                            </thead>
                            <tbody>

                                {this.state.count == 0 && <tr ><td colSpan="15" style={{ textAlign: "center" }}>No Result</td></tr>}
                                {this.state.PaymentData.map((item, i) => {


                                    return <tr key={i} id={"doctor_" + item.id}>
                                        <td ><div> {item.id}	</div></td>
                                        <td ><div> {item.merref}	</div></td>
                                        <td ><div>{item.amount}</div></td>
                                        <td ><div> {item.phone_card_sn}	</div></td>
                                        <td ><div> {item.trans_date}	</div></td>
                                        <td ><div> {item.state}</div></td>
                                        <td ><div> {item.note}</div></td>
                                        <td ><div> {item.created_date}	</div></td>
                                        <td ><div> {item.write_date}	</div></td>
                                        <td ><div> {item.type}</div></td>
                                        <td ><div> {item.payment_gateway}	</div></td>
                                        <td ><div>  {item.user_wallet_id}	</div></td>
                                        <td ><div>  {item.gateway_trans_code}	</div></td>
                                        <td ><div>  {item.customer_token}	</div></td>
                                        <td ><div>  {item.user_payment_info_id}</div></td>

                                    </tr>
                                })}

                            </tbody>
                        </table>
                    </div>
                </div>
                <div className="card-footer small text-muted">...</div>
            </div>
        );
    }
}

export default Payments;