import React from "react";
import { NavLink } from "react-router-dom";

 

//import $ from "jquery";
const $ = window.$;
const editable = window.editable;
const select2 = window.select2;

const swal = window.swal;
class Getdoctors extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      isLogged: false,
      UserData: [],
      idDoctor: 0,
      countDoctor: null,
    }
  }
  componentDidMount() {

    swal(
      'loading',
      'You clicked the button!',
      'success'
    );
    $.ajax({
      url: process.env.REACT_APP_API_URL +'api/doctors/',
      type: 'GET',

      success: (s) => {
        var response = JSON.parse(s);
        if (response.status == "success") {
          var data = response.result;
          this.setState({ UserData: data });
          this.callEditable();
          swal.close();
        } else {
          swal(
            'Error!',
            response.status,
            'error'
          )
        }


      }
    });

  }

  search(e) {
    e.preventDefault();
    $.ajax({
      url: process.env.REACT_APP_API_URL +'api/doctors/search',
      type: 'GET',
      data: {
        search: $("#query").val(),
      },
      success: (s) => {
        var response = JSON.parse(s);
        if (response.status == "success") {
          var data = response.result;
          var count = data.length;
          this.setState({ UserData: data ,countDoctor: count});
          this.callEditable();
          swal.close();
        } else {
          swal(
            'Error!',
            response.status,
            'error'
          )
        }
      }, error: (e) => {
        console.log(e);
      }
    });
  }
  callEditable() {
    $('.editDoctor').editable({
      url: process.env.REACT_APP_API_URL +'api/users/',
      type: 'post',

      params: (params) => {
        console.log(params);
        return params;
      },
      success: (s) => {

        console.log(s);
      },
      error: (e) => {
        console.log(e);
      }
    });

    $('.editDoctorBool').editable({
      url: process.env.REACT_APP_API_URL +'api/users/',
      type: 'post',
      source: "[{value: '1', text: 'true'}, {value: '0', text: 'false'}]",
      params: (params) => {
        console.log(params);
        if (params.value == 1) {
          params.value = true;
        } else {
          params.value = false;
        }
        return params;
      },
      success: (s) => {

        console.log(s);
      },
      error: (e) => {
        console.log(e);
      }
    });

    $('.editDoctorValid').editable({
      url: process.env.REACT_APP_API_URL +'api/users/',
      type: 'post',
      source: "[{value:'active',text:'active'},{value:'inactive',text:'inactive'}]",
      params: (params) => {
        console.log(params);

        return params;
      },
      success: (s) => {

        console.log(s);
      },
      error: (e) => {
        console.log(e);
      }
    });

  }
  changeStatus(idDoc) {
    var valDoc = $("#validation_" + idDoc).text();
    var BoolAct = "inactive";
    if (valDoc == "active") {
      BoolAct ="inactive";
    } else {
      BoolAct ="active";
    }
    swal({
      title: "Do you want to " + BoolAct  + " ?",
      text: "",
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes'
    }).then((result) => {
      console.log(result);
      if(result.value == true)
      {
        $.ajax({
          url: process.env.REACT_APP_API_URL +'api/users',
          type: 'POST',
          data: {
            pk: idDoc,
            name: "validation_status",
            value: valDoc
          },
          success: (s) => {
  
            if (valDoc == "active") {
              $("#validation_" + idDoc).text("inactive");
            } else {
              $("#validation_" + idDoc).text("active");
            }
  
          }, error: (e) => {
            console.log(e);
          }
        });
  
      }
          })
  }
  render() {
    return (

        <div className="content-wrapper">
          <form className="form-inline" style={{ "padding": "10px" }} onSubmit={this.search.bind(this)}>
            <div className="input-group">
              <input className="form-control" type="text" name="query" id="query" placeholder="Search User with Email Or Phone" />
              <span className="input-group-btn">
                <button className="btn btn-primary" type="button">
                  <i className="fa fa-search"></i>
                </button>
              </span>
            </div>
          </form>
          <div className="card-header">
            <i className="fa fa-table"></i> Data Doctor</div>
          <div className="card-body">
            <div className="table-responsive">
              <table className="table table-bordered" id="dataTable" width="100%" cellSpacing="0">
                <thead className="card-header">
                  <tr>
                    <th>id</th>
                    <th>created_date</th>
                    <th>Email</th>
                    <th>Email Confirmed</th>
                    <th>Password</th>
                    <th>Phone</th>
                    <th>Phone_confirmed</th>
                    <th>user_mngment_id</th>
                    <th>validation_status</th>
                    <th>user_role</th>
                  </tr>
                </thead>
                <tbody>
                  {this.state.countDoctor == 0 && <tr ><td colSpan="9" style={{textAlign: "center"}}>Not Found</td></tr>}
                  {this.state.UserData.map((item, i) => {
                    let phoneConfirmation = 0;
                    if (item.phone_confirmed == true) {
                      phoneConfirmation = 1;
                    }
                    let emailConfirmation = 0;
                    if (item.email_confirmed == true) {
                      emailConfirmation = 1;
                    }
                    let validationStatus = false;
                    if (item.validation_status == "active") {
                      validationStatus = true;
                    }

                    return <tr key={i} id={"doctor_" + item.id}>
                      <td ><div>{item.id}</div></td>
                      <td ><div className="editDoctor" data-type="date" data-name="created_date" data-pk={item.id} data-title="Enter new value">{item.created_date}</div></td>
                      <td ><div className="editDoctor" data-type="text" data-name="email" data-pk={item.id} data-title="Enter new value">{item.email}</div></td>
                      <td ><div className="editDoctorBool" data-type="select" data-name="email_confirmed" data-pk={item.id} data-value={emailConfirmation} data-title="Enter new value">{item.email_confirmed.toString()}</div></td>
                      <td ><div className="editDoctor" data-type="text" data-name="password" data-pk={item.id} data-title="Enter new value">{item.password}</div></td>
                      <td ><div className="editDoctor" data-type="number" data-name="phone" data-pk={item.id} data-title="Enter new value">{item.phone}</div></td>
                      <td><div className="editDoctorBool" data-type="select" data-value={phoneConfirmation} data-name="phone_confirmation" data-pk={item.id} data-title="Enter new value">{item.phone_confirmed.toString()}</div></td>
                      <td>{item.user_mngment_id}</td>
                      {validationStatus && <td><div className="validDoctor" id={"validation_" + item.id} onClick={this.changeStatus.bind(this, item.id)}>active</div></td>}
                      {!validationStatus && <td><div className="validDoctor" id={"validation_" + item.id} onClick={this.changeStatus.bind(this, item.id)}>inactive</div></td>}
                      <td>{item.code}</td>
                    </tr>
                  })}

                </tbody>
              </table>
            </div>
          </div>
          <div className="card-footer small text-muted"></div>
        </div>
    );
  }
}

export default Getdoctors;