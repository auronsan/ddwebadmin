import React from "react";
import { NavLink } from "react-router-dom";

import axios from "axios";
const getQuery = window.getQuery;

axios.defaults.timeout = 10000;


//import $ from "jquery";
const $ = window.$;
const editable = window.editable;
const select2 = window.select2;

const swal = window.swal;
class AppointmentsMedicalRecord extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            isLogged: false,
            MedicalRecordData: [],
            idDoctor: 0,
            count: null,
        }
    }

    componentDidMount() {

        var url = process.env.REACT_APP_API_URL +"api/appointment/medicalRecord";
        var id = getQuery("id");
        if (id) {
            url = process.env.REACT_APP_API_URL +"api/appointment/medicalRecord?id=" + id;
        }
        swal({
             title: 'Loading..',         html: '<div class="inner"><span class="spinner"></span><img src="img/logo-call.png"/></div>',  showConfirmButton:false,          
          
            allowOutsideClick: false,
        }
        );
        axios.get(url)
            .then((s) => {
                var response = s.data;
                console.log(response);
                if (response.status == "success") {
                    var data = response.result;
                    var length = data.length;
                    this.setState({ MedicalRecordData: data, count: length });

                    swal.close();
                } else {
                    swal.close();
                    swal(
                        'Error!',
                        response.status,
                        'error'
                    )
                }
            })
            .catch((error) => {
                swal.close();
                console.log(error);
                swal(
                    'Error!',
                    error.toString(),
                    'error'
                )
            });

    }
    render() {
        return (
            <div className="content-wrapper">

                <div className="card-header">
                    <i className="fa fa-table"></i> Data Medical Record</div>
                <div className="card-body">
                    <div className="table-responsive">
                        <table className="table table-bordered" id="dataTable" width="100%" cellSpacing="0">
                            <thead className="card-header">
                                <tr>
                                    <th>id</th>
                                    <th>create_date		</th>
                                    <th>write_date	</th>
                                    <th>appointment_id</th>
                                    <th>note	</th>
                                    <th>code	</th>
                                    <th>attachment_id	</th>
                                    <th>user_mngment_id		</th>
                                    <th>create_uid	</th>

                                </tr>


                            </thead>
                            <tbody>
                                {this.state.count == 0 && <tr ><td colSpan="9" style={{ textAlign: "center" }}>No Result</td></tr>}
                                {this.state.MedicalRecordData.map((item, i) => {


                                    return <tr key={i} id={"doctor_" + item.id}>
                                        <td ><div>{item.id}</div></td>
                                        <td ><div>{item.create_date}	</div></td>
                                        <td ><div>{item.write_date}	</div></td>
                                        <td ><div>{item.appointment_id}</div></td>
                                        <td ><div>{item.note}	</div></td>
                                        <td ><div>{item.code}	</div></td>
                                        <td ><div>{item.attachment_id}	</div></td>
                                        <td ><div>{item.user_mngment_id}	</div></td>
                                        <td ><div>{item.create_uid}	</div></td>

                                    </tr>
                                })}

                            </tbody>
                        </table>
                    </div>
                </div>
                <div className="card-footer small text-muted"></div>
            </div>
        );
    }
}

export default AppointmentsMedicalRecord;