import React from "react";
import { NavLink } from "react-router-dom";


import axios from "axios";
const getQuery = window.getQuery

//import $ from "jquery";
const $ = window.$;
const editable = window.editable;
const select2 = window.select2;

const swal = window.swal;
class DoctorProfile extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            isLogged: false,
            DoctorData: [],
            idDoctor: 0,
            count: null,
        }
    }
    componentDidMount() {
        var url = process.env.REACT_APP_API_URL +"api/doctors/profile";


        swal({
             title: 'Loading..',         html: '<div class="inner"><span class="spinner"></span><img src="img/logo-call.png"/></div>',  showConfirmButton:false,          
            
            allowOutsideClick: false,
        }
        );
        axios.get(url)
            .then((s) => {
                var response = s.data;
                console.log(response);
                if (response.status == "success") {
                    var data = response.result;
                    var length = data.length;
                    this.setState({ DoctorData: data, count: length });
                    swal.close();
                } else {
                    swal.close();
                    swal(
                        'Error!',
                        response.status,
                        'error'
                    )
                }
            })
            .catch((error) => {
                swal.close();
                console.log(error);
                swal(
                    'Error!',
                    error.toString(),
                    'error'
                )
            });

    }

    refreshData() {
        var url = process.env.REACT_APP_API_URL +"api/users";


        swal({
             title: 'Loading..',         html: '<div class="inner"><span class="spinner"></span><img src="img/logo-call.png"/></div>',  showConfirmButton:false,          
            

            allowOutsideClick: false,
        }
        );
        axios.get(url)
            .then((s) => {
                var response = s.data;
                console.log(response);
                if (response.status == "success") {
                    var data = response.result;
                    var length = data.length;
                    this.setState({ DoctorData: data, count: length });

                    swal.close();
                } else {
                    swal.close();
                    swal(
                        'Error!',
                        response.status,
                        'error'
                    )
                }
            })
            .catch((error) => {
                swal.close();
                console.log(error);
                swal(
                    'Error!',
                    error.toString(),
                    'error'
                )
            });

    }
    render() {
        return (

            <div className="content-wrapper" >


                <div className="card-header">
                    <i className="fa fa-table"></i> Profile Doctor</div>
                <div className="card-body" >
                    <div className="table-responsive">
                        <table className="table table-bordered" id="dataTable" width="100%" cellSpacing="0">
                            <thead className="card-header">
                                <tr>
                                    <th>id</th>
                                    <th>user_mngment_id	</th>
                                    <th>consultation_type	</th>
                                    <th>consultation_fee	</th>
                                    <th>consultation_duration	</th>
                                    <th>years_of_experience	</th>
                                    <th>graduation_date	</th>
                                    <th>medical_school	</th>
                                    <th>post_graduate_training</th>
                                    <th>industry_relationship	</th>
                                    <th>publications	</th>
                                    <th>teaching	</th>
                                    <th>community_service	</th>
                                    <th>criminal_convictions	</th>
                                    <th>limitations	</th>
                                    <th>hospital_restrictions	</th>
                                    <th>about	</th>
                                    <th>consultation_unit	</th>
                                    <th>sum_rating_point	</th>
                                    <th>count_rating_point	</th>
                                    <th>professional_affiliations_and_activities</th>
                                </tr>
                            </thead>
                            <tbody>
                                {this.state.count == 0 && <tr ><td colSpan="9" style={{ textAlign: "center" }}>Not Found</td></tr>}
                                {this.state.DoctorData.map((item, i) => {


                                    return <tr key={i} id={"doctor_" + item.id}>
                <td ><div>{item.id}	</div></td>
         <td ><div>{item.user_mngment_id	}</div></td>
         <td ><div>{item.consultation_type}	</div></td>
         <td ><div>{item.consultation_fee}	</div></td>
         <td ><div>{item.consultation_duration}	</div></td>
         <td ><div>{item.years_of_experience}	</div></td>
         <td ><div>{item.graduation_date}	</div></td>
         <td ><div>{item.medical_school}	</div></td>
         <td ><div>{item.post_graduate_training}	</div></td>
         <td ><div>{item.industry_relationship	}</div></td>
         <td ><div>{item.publications	}</div></td>
         <td ><div>{item.teaching}	</div></td>
         <td ><div>{item.community_service}	</div></td>
         <td ><div>{item.criminal_convictions}	</div></td>
         <td ><div>{item.limitations	}</div></td>
         <td ><div>{item.hospital_restrictions}	</div></td>
         <td ><div>{item.about}	</div></td>
         <td ><div>{item.consultation_unit}	</div></td>
         <td ><div>{item.sum_rating_point	}</div></td>
         <td ><div>{item.count_rating_point}	</div></td>
         <td ><div>{item.professional_affiliations_and_activities}</div></td>

                                    </tr>
                                })}

                            </tbody>
                        </table>
                    </div>
                </div>
                <div className="card-footer small text-muted"> </div>
            </div>
        );
    }
}

export default DoctorProfile;