import React from "react";
import { NavLink } from "react-router-dom";


import axios from "axios";
const getQuery = window.getQuery

//import $ from "jquery";
const $ = window.$;
const editable = window.editable;
const select2 = window.select2;

const swal = window.swal;
class Promotion extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            isLogged: false,
            UserData: [],
            idDoctor: 0,
            count: null,
        }
    }
    componentDidMount() {
        var url = process.env.REACT_APP_API_URL +"api/users/promoTrans";


        swal({
            title: 'Loading..', html: '<div class="inner"><span class="spinner"></span><img src="img/logo-call.png"/></div>', showConfirmButton: false,

            allowOutsideClick: false,
        }
        );
        axios.get(url)
            .then((s) => {
                var response = s.data;
                console.log(response);
                if (response.status == "success") {
                    var data = response.result;
                    var length = data.length;
                    this.setState({ UserData: data, count: length });
                    swal.close();
                } else {
                    swal.close();
                    swal(
                        'Error!',
                        response.status,
                        'error'
                    )
                }
            })
            .catch((error) => {
                swal.close();
                console.log(error);
                swal(
                    'Error!',
                    error.toString(),
                    'error'
                )
            });

    }

    refreshData() {
        var url = process.env.REACT_APP_API_URL +"api/users";


        swal({
            title: 'Loading..', html: '<div class="inner"><span class="spinner"></span><img src="img/logo-call.png"/></div>', showConfirmButton: false,

            allowOutsideClick: false,
        }
        );
        axios.get(url)
            .then((s) => {
                var response = s.data;
                console.log(response);
                if (response.status == "success") {
                    var data = response.result;
                    var length = data.length;
                    this.setState({ UserData: data, count: length });

                    swal.close();
                } else {
                    swal.close();
                    swal(
                        'Error!',
                        response.status,
                        'error'
                    )
                }
            })
            .catch((error) => {
                swal.close();
                console.log(error);
                swal(
                    'Error!',
                    error.toString(),
                    'error'
                )
            });

    }
    render() {
        return (

            <div className="content-wrapper" >


                <div className="card-header">
                    <i className="fa fa-table"></i> Promotion Transaction</div>
                <div className="card-body" >
                    <div className="table-responsive">
                        <table className="table table-bordered" id="dataTable" width="100%" cellSpacing="0">
                            <thead className="card-header">
                                <tr>
                                    <th>    id	</th>
                                    <th>user_account_id	</th>
                                    <th>promotion_id	</th>
                                    <th>state	</th>
                                    <th>token	</th>
                                    <th>expiry_time</th>
                                </tr>
                            </thead>
                            <tbody>
                                {this.state.count == 0 && <tr ><td colSpan="9" style={{ textAlign: "center" }}>Not Found</td></tr>}
                                {this.state.UserData.map((item, i) => {


                                    return <tr key={i} id={"doctor_" + item.id}>
                                        <td ><div>  {item.id}	</div></td>
                                        <td ><div>  {item.user_account_id}	</div></td>
                                        <td ><div> {item.promotion_id}	</div></td>
                                        <td ><div> {item.state}</div></td>
                                        <td ><div> {item.token}</div></td>
                                        <td ><div> {item.expiry_time}</div></td>
                                    </tr>
                                })}

                            </tbody>
                        </table>
                    </div>
                </div>
                <div className="card-footer small text-muted"></div>
            </div>
        );
    }
}

export default Promotion;