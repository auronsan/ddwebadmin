import React from "react";
import { NavLink } from "react-router-dom";

import axios from "axios";
const getQuery = window.getQuery;
 

//import $ from "jquery";
const $ = window.$;
const editable = window.editable;
const select2 = window.select2;

const swal = window.swal;
class AppointmentsCall extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      isLogged: false,
      CallHistoryData: [],
      idDoctor: 0,
      count: null,
    }
  }
  
  componentDidMount() {
    
    var url = process.env.REACT_APP_API_URL +"api/appointment/callHistory";
   var id = getQuery("id");
    if( id){
     url = process.env.REACT_APP_API_URL +"api/appointment/callHistory?id="+id;
    }
     swal({
        title: 'Loading..',         html: '<div class="inner"><span class="spinner"></span><img src="img/logo-call.png"/></div>',  showConfirmButton:false,          
       
       
       allowOutsideClick: false,
     }
     );
     axios.get(url)
     .then( (s) => {
       var response = s.data;
       console.log(response);
       if (response.status == "success") {
         var data = response.result;
         var length = data.length;
         this.setState({CallHistoryData: data, count: length });
       
         swal.close();
       } else {
         swal.close();
         swal(
           'Error!',
           response.status,
           'error'
         )
       }
     })
     .catch( (error) => {
       swal.close();
       console.log(error);
       swal(
         'Error!',
         error.toString(),
         'error'
       )
     });
 
   }
  render() {
    return (
        <div className="content-wrapper">
        
          <div className="card-header">
            <i className="fa fa-table"></i> Data Call History</div>
          <div className="card-body">
            <div className="table-responsive">
              <table className="table table-bordered" id="dataTable" width="100%" cellSpacing="0">
                <thead className="card-header">
                  <tr>
                    <th>id</th>
                    <th>appointment_id</th>
                    <th>archive_id	</th>
                    <th>start_time	</th>
                    <th>end_time	</th>
                    <th>session_id	</th>
                    <th>token_to	</th>
                    <th>token_from		</th>
                    <th>state		</th>
                    <th>doctor_call_type	</th>
                    <th>patient_call_type	</th>
                    <th>create_date		</th>
                     <th>write_date	</th>
                  </tr>
                </thead>
                <tbody>
                  {this.state.count == 0 && <tr ><td colSpan="9" style={{textAlign: "center"}}>Not Found</td></tr>}
                  {this.state.CallHistoryData.map((item, i) => {
                   

                    return <tr key={i} id={"doctor_" + item.id}>
                      <td ><div>{item.id}</div></td>
                      <td ><div>{item.appointment_id}</div></td>                   
                      <td ><div>{item.archive_id}	</div></td>
                      <td ><div>{item.start_time}	</div></td>
                      <td ><div>{item.end_time}	</div></td>
                      <td ><div>{item.session_id}	</div></td>
                      <td ><div>{item.token_to}	</div></td>
                      <td ><div>{item.token_from}	</div></td>
                      <td ><div>{item.state}	</div></td>
                      <td ><div>{item.doctor_call_type}	</div></td>
                      <td ><div>{item.patient_call_type}	</div></td>
                      <td ><div>{item.create_date}	</div></td>
                      <td ><div>{item.write_date}	</div></td>
                    </tr>
                  })}

                </tbody>
              </table>
            </div>
          </div>
          <div className="card-footer small text-muted"></div>
        </div>
    );
  }
}

export default AppointmentsCall;