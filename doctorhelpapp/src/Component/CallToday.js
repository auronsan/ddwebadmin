import React from "react";
import { NavLink } from "react-router-dom";
import axios from "axios";
const getQuery = window.getQuery

//import $ from "jquery";
const $ = window.$;
const editable = window.editable;
const select2 = window.select2;

const swal = window.swal;
class CallToday extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            isLogged: false,
            CallDetail: [],
            idDoctor: 0,
            count: null,
        }
    }
    componentDidMount() {
        this.fetchData();
        
    }
    fetchData(){
        var url = process.env.REACT_APP_API_URL +"api/getcalltodaydetail";


        swal({
            title: 'Loading..', html: '<div class="inner"><span class="spinner"></span><img src="img/logo-call.png"/></div>', showConfirmButton: false,

            allowOutsideClick: false,
        }
        );
        axios.get(url)
            .then((s) => {
                var response = s.data;
                console.log(response);
                if (response.status == "success") {
                    var data = response.result;
                    var length = data.length;
                    this.setState({ CallDetail: data, count: length });
  
                    swal.close();
                } else {
                    swal.close();
                    swal(
                        'Error!',
                        response.status,
                        'error'
                    )
                }
            })
            .catch((error) => {
                swal.close();
                console.log(error);
                swal(
                    'Error!',
                    error.toString(),
                    'error'
                )
            });

    }
    render() {
        return (

            <div className="content-wrapper" >


                <div className="card-header">
                    <i className="fa fa-table"></i>Call today </div>

                <div className="card-body" >
                    <div className="table-responsive">
                        <table className="table table-bordered" id="dataTable" width="100%" cellSpacing="0">
                            <thead className="card-header">
                                <tr>
                                    <th>    id	</th>
                                    <th> patient_id	</th>
                                    <th> doctor_id	</th>
                                    <th>    status_call	</th>
                                    <th> last_update	</th>
                                    <th> apm_id	</th>
                                </tr>
                            </thead>
                            <tbody>
                                {this.state.count == 0 && <tr ><td colSpan="9" style={{ textAlign: "center" }}>No Call today</td></tr>}
                                {this.state.CallDetail.map((item, i) => {
                                    return <tr key={i} id={"doctor_" + item.id}>
                                        <td ><div>  {item.id}	</div></td>
                                        <td ><div>  {item.patient_id}	</div></td>
                                        <td ><div>  {item.doctor_id}	</div></td>
                                        <td ><div>  {item.status_call}	</div></td>
                                        <td ><div>  {item.last_update}	</div></td>
                                        <td ><div>  {item.apm_id}	</div></td>
                                        </tr>
                                })}

                            </tbody>
                        </table>
                    </div>
                </div>
                <div className="card-footer small text-muted"></div>
            </div>
        );
    }
}

export default CallToday;