import React from "react";
import { NavLink } from "react-router-dom";


import axios from "axios";
const getQuery = window.getQuery

//import $ from "jquery";
const $ = window.$;
const editable = window.editable;
const select2 = window.select2;

const swal = window.swal;
class DoctorLicense extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            isLogged: false,
            DoctorData: [],
            idDoctor: 0,
            count: null,
        }
    }
    componentDidMount() {
        var url = process.env.REACT_APP_API_URL +"api/doctors/license";


        swal({
            title: 'Loading..', html: '<div class="inner"><span class="spinner"></span><img src="img/logo-call.png"/></div>', showConfirmButton: false,

            allowOutsideClick: false,
        }
        );
        axios.get(url)
            .then((s) => {
                var response = s.data;
                console.log(response);
                if (response.status == "success") {
                    var data = response.result;
                    var length = data.length;
                    this.setState({ DoctorData: data, count: length });
                    swal.close();
                } else {
                    swal.close();
                    swal(
                        'Error!',
                        response.status,
                        'error'
                    )
                }
            })
            .catch((error) => {
                swal.close();
                console.log(error);
                swal(
                    'Error!',
                    error.toString(),
                    'error'
                )
            });

    }

    render() {
        return (

            <div className="content-wrapper" >


                <div className="card-header">
                    <i className="fa fa-table"></i>Doctor License</div>
                <div className="card-body" >
                    <div className="table-responsive">
                        <table className="table table-bordered" id="dataTable" width="100%" cellSpacing="0">
                            <thead className="card-header">
                                <tr>
                                    <th>    id	</th>

                                    <th>number	</th>
                                    <th>license_status	</th>
                                    <th>issue_date	</th>
                                    <th>expiry_date	</th>
                                    <th>issue_by	</th>
                                    <th>doctor_profile_id</th>
                                </tr>
                            </thead>
                            <tbody>
                                {this.state.count == 0 && <tr ><td colSpan="9" style={{ textAlign: "center" }}>Not Found</td></tr>}
                                {this.state.DoctorData.map((item, i) => {


                                    return <tr key={i} id={"doctor_" + item.id}>

                                        <td ><div>{item.id}</div></td>
                                        <td ><div>{item.number}</div></td>
                                        <td ><div> {item.license_status}</div></td>
                                        <td ><div>{item.issue_date}</div></td>
                                        <td ><div>{item.expiry_date}</div></td>
                                        <td ><div>{item.issue_by}</div></td>
                                        <td ><div>{item.doctor_profile_id}</div></td>

                                    </tr>
                                })}

                            </tbody>
                        </table>
                    </div>
                </div>
                <div className="card-footer small text-muted"></div>
            </div>
        );
    }
}

export default DoctorLicense;