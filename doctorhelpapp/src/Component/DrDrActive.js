import React from "react";
import { NavLink } from "react-router-dom";
import axios from "axios";

const getQuery = window.getQuery

//import $ from "jquery";
const $ = window.$;
const editable = window.editable;
const select2 = window.select2;

const swal = window.swal;
class UserWallet extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            isLogged: false,
            DDCardData: [],
            idDoctor: 0,
            count: null,
        }
    }
    componentDidMount() {
        this.fetchData();
        
    }
    fetchData(){
        var url = process.env.REACT_APP_API_URL +"api/ddcard/getddcardactive/";


        swal({
            title: 'Loading..', html: '<div class="inner"><span class="spinner"></span><img src="img/logo-call.png"/></div>', showConfirmButton: false,

            allowOutsideClick: false,
        }
        );
        axios.get(url)
            .then((s) => {
                var response = s.data;
                console.log(response);
                if (response.status == "success") {
                    var data = response.result;
                    var length = data.length;
                    this.setState({ DDCardData: data, count: length });
  
                    swal.close();
                } else {
                    swal.close();
                    swal(
                        'Error!',
                        response.status,
                        'error'
                    )
                }
            })
            .catch((error) => {
                swal.close();
                console.log(error);
                swal(
                    'Error!',
                    error.toString(),
                    'error'
                )
            });

    }
    generateddd(){
        swal({
            html: 'Quantity : <input id="number" class="input__data" type="number"><br/>Amount : <select id="voucher"  class="input__data" >' +
            '<option value="50">50000</option><option value="100">100000</option><option value="200">200000</option></select>' +
            '<br/>Gen_by : <select id="genby"  class="input__data" ><option value="1">Admin</option></select>',
            showCancelButton: true,
            confirmButtonText: 'Generate',
            showLoaderOnConfirm: true,
            preConfirm: (data) => {
               if(data) {
                var number = $('#number').val();
                var voucher = $('#voucher').val();
                var genby = $('#genby').val();
                var result = [number,voucher,genby];
                return result;
               }
              
            },
            allowOutsideClick: () => !swal.isLoading()
          }).then((result) => {
              console.log(result)
              var url = process.env.REACT_APP_DD_CARD_URL +"admin/ddcard/gen?api_key="+process.env.REACT_APP_DD_ADMIN_API_KEY+"&auto=1&number="+result.value[0]+"&voucher="+result.value[1]+"&role_id="+result.value[2];
              //var url = process.env.REACT_APP_DD_CARD_URL +"/hospital/admin/ddcard/generate?auto=1&number="+result.value[0]+"&voucher="+result.value[1]+"";
              console.log(url);
              axios.get(url)
            .then((s) => {
                console.log(s);
                this.fetchData();
              
            });
            
          })
        
    }
    render() {
        return (

            <div className="content-wrapper" >


                <div className="card-header">
                    <i className="fa fa-table"></i>dd card active</div>

                <div className="card-body" >
               
                    <div className="table-responsive">
                        <table className="table table-bordered" id="dataTable" width="100%" cellSpacing="0">
                            <thead className="card-header">
                                <tr>
                                    <th>    id	</th>
                                    <th> pdf_file	</th>
                                    <th>Action	</th>
                                </tr>
                            </thead>
                            <tbody>
                                {this.state.count == 0 && <tr ><td colSpan="9" style={{ textAlign: "center" }}>Not Found</td></tr>}
                                {this.state.DDCardData.map((item, i) => {
                                    return <tr key={i} id={"doctor_" + item.id}>
                                        <td ><div>  {item.id}	</div></td>
                                        <td ><div>  {item.pdf_file}	</div></td>
                                        <td ><div>  <a href={process.env.REACT_APP_DD_CARD_URL + "static/" + item.pdf_file} >Download</a>	</div></td>  </tr>
                                })}

                            </tbody>
                        </table>
                    </div>
                </div>
                <div className="card-footer small text-muted"></div>
            </div>
        );
    }
}

export default UserWallet;