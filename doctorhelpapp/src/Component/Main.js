import React, { Component } from "react";
import { Route, HashRouter } from "react-router-dom";
import Login from "./Login";
import Home from "./Home";
import Getdoctors from "./Getdoctors";
import Getpatients from "./Getpatients";
import Getusers from "./Getusers";
import Appointments from "./Appointments";
import AppointmentsCall from "./AppointmentsCall";
import AppointmentsMedicalRecord from "./AppointmentsMedicalRecord";
import AppointmentsReview from "./AppointmentsReview";
import AppointmentsTransaction from "./AppointmentsTransaction";

import BusinessTransfer from "./BusinessTransfer";

import Payments from "./Payments";


import DoctorProfile from "./DoctorProfile";

import DoctorSpecialties from "./DoctorSpecialties";

import DoctorLanguages from "./DoctorLanguages";

import DoctorLicense from "./DoctorLicense";

import GeneralMedicalRecord from "./GeneralMedicalRecord";

import Promotion from "./Promotion";

import UserWallet from "./UserWallet";

import DDCardManagement from "./DDCardManagement";

import DrDrCardList from "./DrDrCardList";

import DrDrActive from "./DrDrActive";


import CallToday from "./CallToday";
import StripeToday from "./StripeToday";
import DrDrToday from "./DrDrToday";
import Navbar from "./Navbar";
import Cookies from 'universal-cookie';

const $ = window.jQuery;
window.objLength = function objLength(obj){
  var i=0;
  for (var x in obj){
    if(obj.hasOwnProperty(x)){
      i++;
    }
  } 
  return i;
}


const cookies = new Cookies();
class Main extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isLogged: false,
      company_id: 0
    }
  }

  componentDidMount() {
    if (cookies.get('tokenAdminDD')) {
      this.setState({ isLogged: true });
      //window.location.href = "#home";
    } else {

    }
    /* if (cookies.get('tokenHrforte')) {
       window.location.href = "#home";
       this.setState({ isLogged: true });
     } else {
       console.log('Not login');
     }*/
  }
  setStateLogged() {
    if (this.state.isLogged) {
      cookies.remove('tokenAdminDD');
      this.setState({ isLogged: false });

    } else {
      this.setState({ isLogged: true });
      cookies.set('tokenAdminDD', 'true', { path: '/' });
      //window.location.href = "#home";

    }
  }
  setStateCompany(companyID) {
    if (companyID) {
      this.setState({ company_id: companyID });
    }
  }
  logout() {
    this.setStateLogged();
    $("#exampleModal").modal('toggle');
  }
  render() {
    const currentRoute = this.props;
    console.log(currentRoute);
    return (
      <HashRouter>

        <div>
          <div className="content">
            <div className="fixed-nav sticky-footer bg-dark" id="page-top">
              {this.state.isLogged && <Navbar />}

              {this.state.isLogged && <Route path="/home" component={Home} />}
              {this.state.isLogged && <Route path="/Getdoctors" component={Getdoctors} />}

              {this.state.isLogged && <Route path="/Getpatients" component={Getpatients} />}

              {this.state.isLogged && <Route path="/Getusers" component={Getusers} />}

              {this.state.isLogged && <Route path="/Appointments" component={Appointments} />}


              {this.state.isLogged && <Route path="/AppointmentsCall" component={AppointmentsCall} />}

              {this.state.isLogged && <Route path="/AppointmentsMedicalRecord" component={AppointmentsMedicalRecord} />}

              {this.state.isLogged && <Route path="/AppointmentsReview" component={AppointmentsReview} />}

              {this.state.isLogged && <Route path="/AppointmentsTransaction" component={AppointmentsTransaction} />}


              {this.state.isLogged && <Route path="/BusinessTransfer" component={BusinessTransfer} />}

              {this.state.isLogged && <Route path="/Payments" component={Payments} />}

              {this.state.isLogged && <Route path="/DoctorProfile" component={DoctorProfile} />}

              {this.state.isLogged && <Route path="/DoctorLanguages" component={DoctorLanguages} />}

              {this.state.isLogged && <Route path="/DoctorSpecialties" component={DoctorSpecialties} />}

              {this.state.isLogged && <Route path="/GeneralMedicalRecord" component={GeneralMedicalRecord} />}

              {this.state.isLogged && <Route path="/Promotion" component={Promotion} />}

              {this.state.isLogged && <Route path="/DoctorLicense" component={DoctorLicense} />}

              {this.state.isLogged && <Route path="/UserWallet" component={UserWallet} />}
              
              {this.state.isLogged && <Route path="/ddcardmanagement" component={DDCardManagement} />}

              {this.state.isLogged && <Route path="/calldetailtoday" component={CallToday} />}
              
              {this.state.isLogged && <Route path="/stripedetailtoday" component={StripeToday} />}
              
              {this.state.isLogged && <Route path="/drdrdetailtoday" component={DrDrToday} />}


              {this.state.isLogged && <Route path="/drdrcardlist" component={DrDrCardList} />}

              {this.state.isLogged && <Route path="/drdrcardactive" component={DrDrActive} />}
              

              {!this.state.isLogged && <Route path='*' exact={true} render={() => <Login setLogged={this.setStateLogged.bind(this)} />} />}

            </div>
          </div>
          <div className="modal fade" id="exampleModal" tabIndex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div className="modal-dialog" role="document">
              <div className="modal-content">
                <div className="modal-header">
                  <h5 className="modal-title" id="exampleModalLabel">Ready to Leave?</h5>
                  <button className="close" type="button" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                  </button>
                </div>
                <div className="modal-body">Select "Logout" below if you are ready to end your current session.</div>
                <div className="modal-footer">
                  <button className="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
                  <a className="btn btn-primary" onClick={this.logout.bind(this)}>Logout</a>
                </div>
              </div>
            </div>
          </div>
        </div>
      </HashRouter>
    );
  }
}

export default Main;