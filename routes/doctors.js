var express = require('express');
var router = express.Router();

var bodyParser = require('body-parser');
let client = new pg.Client(conString);
 
router.use(bodyParser.json());
router.get('/search/', async (req, res, next) => {
   
  console.log(req.query);
  try{
    client.connect();
    var query = "SELECT account.*,role.code FROM user_account  as account INNER JOIN user_account_user_role as user_role ON account.id = user_role.user_account_id INNER JOIN user_role as role ON user_role.user_role_id = role.id where role.code = 'doctor' AND (LOWER(account.email) like LOWER('%"+req.query.search+"%') or account.phone like '%"+req.query.search+"%') ;";
    console.log(query);
    var result = await client.query(query);
    var resultJson = result.rows;
    client.end();
    res.send(JSON.stringify({ status: "success", result: resultJson  }));
  }catch (err){
    client.end();

    res.send(JSON.stringify({ status: "error", result: err }));
  
}
  
});

router.get('/profile', async (req, res, next) => {
  
 
 try{
   client.connect();
   var resultJson = [];
   var query = "SELECT * from doctor_profile;";
   if(req.query.id !="" && req.query.id !=null ){   
       query = "SELECT  * from doctor_profile where user_mngment_id='"+req.query.id+"';";
   }
 
   var result = await client.query(query);
   var resultJson = result.rows;
   client.end();
   
   res.send(JSON.stringify({ status: "success", result: resultJson  }));
 }catch (err){
   res.send(JSON.stringify({ status: "error", result: err }));
 
}
 
});


router.get('/profileLang', async (req, res, next) => {
  
 
 try{
   client.connect();
   var query = "SELECT * from dr_doctorprofile_languages;";
   if(req.query.id !="" && req.query.id !=null ){
      queryId = "SELECT  * from doctor_profile where user_mngment_id='"+req.query.id+"';";
      var resultId = await client.query(queryId);
      var idDoctor = resultId.rows[0].id;
       query = "SELECT  * from dr_doctorprofile_languages where doctor_profile_id='"+idDoctor+"';";
   }
 
   var result = await client.query(query);
   
   var resultJson = result.rows;
   res.send(JSON.stringify({ status: "success", result: resultJson  }));
 }catch (err){
   res.send(JSON.stringify({ status: "error", result: err }));
 
}
 
});

router.get('/license', async (req, res, next) => {
  
 
 try{
   client.connect();
   var query = "SELECT * from dr_license;";
   if(req.query.id !="" && req.query.id !=null ){   
      queryId = "SELECT  * from doctor_profile where user_mngment_id='"+req.query.id+"';";
      var resultId = await client.query(queryId);
      var idDoctor = resultId.rows[0].id;
       query = "SELECT  * from dr_license where doctor_profile_id='"+idDoctor+"';";
   }
   
   var result = await client.query(query);
   
   var resultJson = result.rows;
   res.send(JSON.stringify({ status: "success", result: resultJson  }));
 }catch (err){
   res.send(JSON.stringify({ status: "error", result: err }));
 
}
 
});


router.get('/profileSpecial', async (req, res, next) => {
  
 
 try{
   client.connect();
   var query = "SELECT * from dr_doctorprofile_specialties;";
   if(req.query.id !="" && req.query.id !=null ){   
      queryId = "SELECT  * from doctor_profile where user_mngment_id='"+req.query.id+"';";
      var resultId = await client.query(queryId);
      var idDoctor = resultId.rows[0].id;
       query = "SELECT  * from dr_doctorprofile_specialties where doctor_profile_id='"+idDoctor+"';";
   }
   
   var result = await client.query(query);
   
   var resultJson = result.rows;
   res.send(JSON.stringify({ status: "success", result: resultJson  }));
 }catch (err){
   res.send(JSON.stringify({ status: "error", result: err }));
 
}
 
});

router.get('/', async (req, res, next) => {
  
 
 try{
   client.connect();
   var result = await client.query("SELECT account.*,role.code FROM user_account  as account INNER JOIN user_account_user_role as user_role ON account.id = user_role.user_account_id INNER JOIN user_role as role ON user_role.user_role_id = role.id where role.code = 'doctor'" );
   var resultJson = result.rows;
   client.end();
   
   res.send(JSON.stringify({ status: "success", result: resultJson  }));
 }catch (err){
  client.end();

   res.send(JSON.stringify({ status: "error", result: err }));
 
}
 
});


router.post('/', async (req, res) => {
  var client = new pg.Client(conString);
   
  console.log(req.body);
  try{
    client.connect();
    var query = "UPDATE user_account SET "+req.body.name+"='"+req.body.value+"' where id="+req.body.pk;
    console.log(query);
    client.query(query);
    client.end();
    
    res.send(JSON.stringify({ status: "success", result: req.body }));
  }catch (err){
    client.end();

    res.send(JSON.stringify({ status: "error", result: err }));
  
}


});
module.exports = router;
