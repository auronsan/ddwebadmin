var express = require('express');
var router = express.Router();


var bodyParser = require('body-parser');
router.use(bodyParser.json());
/* GET home page. */


router.get('/getddfilelist', async (req, res, next) => {
  try {
     
      var resultJson = [];
      let hospital_id = 1;
      if(req.query.hospital_id){
        hospital_id = req.query.hospital_id;
      }
      var query = "select * from dd_card_file_list ";
      console.log(query)
      var result = await database.query(query);
      resultJson = result.rows;
      res.send(JSON.stringify({ status: "success", result: resultJson }));
  } catch (err) {
      res.send(JSON.stringify({ status: "error", result: err }));

  }

});

router.get('/getddcardactive', async (req, res, next) => {
  try {
     
      var resultJson = [];
      let hospital_id = 1;
      if(req.query.hospital_id){
        hospital_id = req.query.hospital_id;
      }
      var query = "select * from dd_card_active ";
      console.log(query)
      var result = await database.query(query);
      resultJson = result.rows;
      res.send(JSON.stringify({ status: "success", result: resultJson }));
  } catch (err) {
      res.send(JSON.stringify({ status: "error", result: err }));

  }

});

router.get('/getddcardlist', async (req, res, next) => {
  try {
     
      var resultJson = [];
      let hospital_id = 1;
      if(req.query.hospital_id){
        hospital_id = req.query.hospital_id;
      }
      var query = "select * from dd_card ";
      console.log(query)
      var result = await database.query(query);
      resultJson = result.rows;
      res.send(JSON.stringify({ status: "success", result: resultJson }));
  } catch (err) {
      res.send(JSON.stringify({ status: "error", result: err }));

  }

});

module.exports = router;
