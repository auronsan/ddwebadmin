var express = require('express');
var router = express.Router();

var bodyParser = require('body-parser');

router.use(bodyParser.json());
router.get('/', async (req, res, next) => {
   
  console.log(req.query);
  try{
    var client = new pg.Client(conString);
    client.connect();
    var resultJson = [];
    if(req.query.id)
    {
      var query = "SELECT * from appointment where doctor_id="+req.query.id+" or patient_id="+req.query.id+";";
      
     
    }else{
      var query = "SELECT * from appointment;";
      
    }
    console.log(query);
    var result = await client.query(query);
    resultJson = result.rows;
    client.end();
    
    res.send(JSON.stringify({ status: "success", result: resultJson  }));
  }catch (err){client.end();
    res.send(JSON.stringify({ status: "error", result: err }));
  
}
  
});


router.get('/all/', async (req, res, next) => {
   
  console.log(req.query);
  try{
    var client = new pg.Client(conString);
    client.connect();
    var resultJson = [];
      var query = "SELECT * from appointment ;";
      console.log(query);
      var result = await client.query(query);
      resultJson = result.rows;
      client.end();    
    res.send(JSON.stringify({ status: "success", result: resultJson  }));
    
  }catch (err){client.end();
    res.send(JSON.stringify({ status: "error", result: err }));
  
}
  
});


router.get('/callHistory/', async (req, res, next) => {
   
  
  try{
    var client = new pg.Client(conString);
    var resultJson = [];
    var result = [];
    client.connect();
    if(req.query.id == null)
    {
      result = await client.query("SELECT * from appointment_call_history ;");   
      resultJson = result.rows;
      
    }else{
      result = await client.query("SELECT * from appointment_call_history where appointment_id="+req.query.id);   
      resultJson = result.rows;
    
    }
  
    client.end();
    res.send(JSON.stringify({ status: "success", result: resultJson  }));
  }catch (err){client.end();
    res.send(JSON.stringify({ status: "error", result: err }));
  
}
  
});


router.get('/medicalRecord/', async (req, res, next) => {
  var client = new pg.Client(conString);
     
    
    try{
      client.connect();
      var result = await client.query("SELECT * from appointment_medical_record");
      var resultJson = result.rows;
      client.end();
      res.send(JSON.stringify({ status: "success", result: resultJson  }));
    }catch (err){client.end();
      res.send(JSON.stringify({ status: "error", result: err }));
      
  }
   
  
  });
  
  
router.get('/review/', async (req, res, next) => {
  var client = new pg.Client(conString);
     
  try{
    var resultJson = [];
    var result = [];
    client.connect();
    if(req.query.id == null)
    {
      result = await client.query("SELECT * from appointment_review ;");   
      resultJson = result.rows;
      
    }else{
      result = await client.query("SELECT * from appointment_review where appointment_id="+req.query.id);   
      resultJson = result.rows;
    
    }
    client.end();
  
    
    res.send(JSON.stringify({ status: "success", result: resultJson  }));
  }catch (err){client.end();
    res.send(JSON.stringify({ status: "error", result: err }));
  
}
    
  });
  
  
  
router.get('/transaction/', async (req, res, next) => {
     
  var client = new pg.Client(conString);
    
  try{
    var resultJson = [];
    var result = [];
    client.connect();
    if(req.query.id == null)
    {
      result = await client.query("SELECT * from appointment_transaction ;");   
      resultJson = result.rows;
      
    }else{
      result = await client.query("SELECT * from appointment_transaction where appointment_id="+req.query.id);   
      resultJson = result.rows;
    
    }
  
    client.end();
    
    res.send(JSON.stringify({ status: "success", result: resultJson  }));
  }catch (err){
    client.end();

    res.send(JSON.stringify({ status: "error", result: err }));
    
  
}

});
module.exports = router;
