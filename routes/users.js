var express = require('express');
var router = express.Router();

var bodyParser = require('body-parser');

router.use(bodyParser.json());
router.get('/search/', async (req, res, next) => {
  console.log(req.query);
  try{
    var client = new pg.Client(conString);
    client.connect();
    var query = "SELECT account.*,role.code FROM user_account  as account INNER JOIN user_account_user_role as user_role ON account.id = user_role.user_account_id INNER JOIN user_role as role ON user_role.user_role_id = role.id where LOWER(account.email) like LOWER('%"+req.query.search+"%') or account.phone like '%"+req.query.search+"%';";
    console.log(query);
    var result = await client.query(query);
    var resultJson = result.rows;
    
    res.send(JSON.stringify({ status: "success", result: resultJson  }));
  }catch (err){
    res.send(JSON.stringify({ status: "error", result: err }));
  
}
  
});



router.get('/promoTrans', async (req, res, next) => {
  
 
 try{
   var client = new pg.Client(conString);
   client.connect();
   var query = "SELECT * from promotion_transaction;";
   if(req.query.id !="" && req.query.id !=null ){   
       query = "SELECT  * from promotion_transaction where user_account_id='"+req.query.id+"';";
   }
 
   var result = await client.query(query);

   
   var resultJson = result.rows;
   res.send(JSON.stringify({ status: "success", result: resultJson  }));
 }catch (err){
   res.send(JSON.stringify({ status: "error", result: err }));
 
}
 
});
router.put('/userWallet', async (req, res, next) => {
  
 
 try{
   var client = new pg.Client(conString);
   client.connect();
   var query = "UPDATE user_wallet SET "+req.body.name+"="+req.body.value+" where id="+req.body.pk;
   console.log(query);
   var result = await  client.query(query);

   
   var resultJson = result.rows;
   res.send(JSON.stringify({ status: "success", result: resultJson  }));
 }catch (err){
   res.send(JSON.stringify({ status: "error", result: err }));
 
}
 
});
router.get('/userWallet', async (req, res, next) => {
  
 
 try{
   var client = new pg.Client(conString);
   client.connect();
   var query = "SELECT * from user_wallet;";
   if(req.query.id !="" && req.query.id !=null ){   
       query = "SELECT  * from user_wallet where user_account_id='"+req.query.id+"';";
   }
 
   var result = await client.query(query);

   
   var resultJson = result.rows;
   res.send(JSON.stringify({ status: "success", result: resultJson  }));
 }catch (err){
   res.send(JSON.stringify({ status: "error", result: err }));
 
}
 
});
router.get('/', async (req, res, next) => {
  
  try{
    var client = new pg.Client(conString);
    client.connect();
    var result = await client.query("SELECT account.*,role.code FROM user_account  as account INNER JOIN user_account_user_role as user_role ON account.id = user_role.user_account_id INNER JOIN user_role as role ON user_role.user_role_id = role.id");
    var resultJson = result.rows;
    
    res.send(JSON.stringify({ status: "success", result: resultJson  }));
  }catch (err){
    res.send(JSON.stringify({ status: "error", result: err }));
  
}
  
});

router.put('/', async (req, res) => {
  console.log(req.body);
  try{
    var client = new pg.Client(conString);
    client.connect();
    var query = "UPDATE user_account SET "+req.body.name+"='"+req.body.value+"' where id="+req.body.pk;
    console.log(query);
    var result = await  client.query(query);
    
    res.send(JSON.stringify({ status: "success", result: result }));
  }catch (err){
    res.send(JSON.stringify({ status: "error", result: err }));
  
}


});

router.put('/role', async (req, res) => {
  console.log(req.body);
  try{
    var client = new pg.Client(conString);
    client.connect();

    var queryRole = "select * from user_role where code='"+req.body.value+"';";
    
    var resultRole = await client.query(queryRole);
    var idRoleUser = resultRole.rows[0].id;
    var query = "UPDATE user_account_user_role SET user_role_id='"+idRoleUser+"' where user_account_id="+req.body.pk;
    console.log(query);
    var result = await  client.query(query);
    
    res.send(JSON.stringify({ status: "success", result: result }));
  }catch (err){
    res.send(JSON.stringify({ status: "error", result: err }));
  
}


});

router.post('/', async (req, res) => {
  console.log(req.body);
  try{
    var client = new pg.Client(conString);
    client.connect();
    var query = "INSERT into user_account (email,phone,password,email_confirmed,phone_confirmed,validation_status) values ('"+req.body.email+"','"+req.body.phone+"','"+req.body.password+"',true,true,'active') RETURNING id";
    console.log(query);
    var result = await client.query(query);
    console.log(result);
    console.log(result.rows);

    console.log(result.rows[0].id);

    var queryRole = "select * from user_role where code='"+req.body.code+"';";
    
    var resultRole = await client.query(queryRole);
   
    var idUser = result.rows[0].id;
    var idRoleUser = resultRole.rows[0].id;

    var query2 = "INSERT into user_account_user_role (user_role_id,user_account_id) values ('"+idRoleUser+"','"+idUser+"');";
    console.log(query2);
    var result2 = await client.query(query2);
    res.send(JSON.stringify({ status: "success", result: req.body }));
  }catch (err){
    res.send(JSON.stringify({ status: "error", result: err }));
  
}




});


router.delete('/', async (req, res) => {
  console.log(req.body);
  try{
    var client = new pg.Client(conString);
    client.connect();
    var query = "delete from user_account where id IN ("+req.body.id+");";
    console.log(query);
    var result = await client.query(query);
    var query1 = "delete from user_account_user_role where user_account_id IN ("+req.body.id+");";
    console.log(query1);
    var result1 = await client.query(query1);
    res.send(JSON.stringify({ status: "success", result: result }));
  }catch (err){
    res.send(JSON.stringify({ status: "error", result: err }));
  
}
});
module.exports = router;
