var express = require('express');
var router = express.Router();

var bodyParser = require('body-parser');

router.use(bodyParser.json());
router.get('/search/', async (req, res, next) => {
   
  console.log(req.query);
  try{
    var client = new pg.Client(conString);
    client.connect();
    var query = "SELECT account.*,role.code FROM user_account  as account INNER JOIN user_account_user_role as user_role ON account.id = user_role.user_account_id INNER JOIN user_role as role ON user_role.user_role_id = role.id where role.code = 'patient' AND (LOWER(account.email) like LOWER('%"+req.query.search+"%') or account.phone like '%"+req.query.search+"%');";
    console.log(query);
    var result = await client.query(query);
    var resultJson = result.rows;
    
    res.send(JSON.stringify({ status: "success", result: resultJson  }));
  }catch (err){
    res.send(JSON.stringify({ status: "error", result: err }));
  
}
  
});


router.get('/generalMedical', async (req, res, next) => {
  
 
 try{
   var client = new pg.Client(conString);
   client.connect();
   var query = "SELECT * from general_medical_record;";
   if(req.query.id !="" && req.query.id !=null ){   
       query = "SELECT  * from general_medical_record where user_mngment_id='"+req.query.id+"';";
   }
 
   var result = await client.query(query);
   
   var resultJson = result.rows;
   res.send(JSON.stringify({ status: "success", result: resultJson  }));
 }catch (err){
   res.send(JSON.stringify({ status: "error", result: err }));
 
}
 
});

router.get('/', async (req, res, next) => {
   
  
  try{
    var client = new pg.Client(conString);
    client.connect();
    var result = await client.query("SELECT account.*,role.code FROM user_account  as account INNER JOIN user_account_user_role as user_role ON account.id = user_role.user_account_id INNER JOIN user_role as role ON user_role.user_role_id = role.id where role.code='patient'" );
    var resultJson = result.rows;
    
    res.send(JSON.stringify({ status: "success", result: resultJson  }));
  }catch (err){
    res.send(JSON.stringify({ status: "error", result: err }));
  
}
  
});

router.post('/', async (req, res) => {
   
  console.log(req.body);
  try{
    var client = new pg.Client(conString);
    client.connect();
    var query = "UPDATE user_account SET "+req.body.name+"='"+req.body.value+"' where id="+req.body.pk;
    console.log(query);
    client.query(query);
    
    res.send(JSON.stringify({ status: "success", result: req.body }));
  }catch (err){
    res.send(JSON.stringify({ status: "error", result: err }));
  
}


});
module.exports = router;
