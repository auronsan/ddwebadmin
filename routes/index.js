var express = require('express');
var router = express.Router();


var bodyParser = require('body-parser');
router.use(bodyParser.json());
/* GET home page. */
router.get('/', async function(req, res, next) {
  res.render('index', { title: 'Express' });
});


router.post('/login', async function(req, res, next) {
  try {
  var email = req.body.email ? req.body.email : 0;
  var password = req.body.password ? req.body.password : 0;
 
  if(email  == 0 || password == 0){
    res.send(JSON.stringify({ status: "error", result: [email,password], message: 'empty email or password' }));
  }else {
    var query = "select ua.id from user_account ua inner join user_account_user_role uaur on uaur.user_account_id = ua.id inner join user_role ur on ur.id = uaur.user_role_id where email = '"+email+"' and password = '"+password+"' and ur.code = 'admin' ";
    console.log(query)
    var result = await database.query(query);
    var resultJson = result.rows;
    if(resultJson.length > 0) {
      res.send(JSON.stringify({ status: "success", result: resultJson }));
    }
    res.send(JSON.stringify({ status: "error", result: [email,password], message: 'invalid' }));
  }
 
} catch (err) {
  res.send(JSON.stringify({ status: "error", result: err }));

}

});


router.get('/getcalltoday', async (req, res, next) => {
  try {
     
      var resultJson = [];
      let hospital_id = 1;
      if(req.query.hospital_id){
        hospital_id = req.query.hospital_id;
      }
      var query = "select count(crl.id),date_part('day',last_update) as timeday,max(last_update)  from call_record_logs crl inner join appointment apm on apm.id = crl.apm_id inner join user_account ua on ua.id = apm.doctor_id inner join doctor_profile dp on dp.user_mngment_id = ua.user_mngment_id where dp.hospital_profile_id = "+hospital_id+" and date_part('day',last_update) = date_part('day',current_timestamp) group by timeday order by timeday asc ";
      console.log(query)
      var result = await database.query(query);
      resultJson = result.rows;
      res.send(JSON.stringify({ status: "success", result: resultJson }));
  } catch (err) {
      res.send(JSON.stringify({ status: "error", result: err }));

  }

});


router.get('/getcalltodaydetail', async (req, res, next) => {
  try {
     
      var resultJson = [];
      let hospital_id = 1;
      if(req.query.hospital_id){
        hospital_id = req.query.hospital_id;
      }
      var query = "select crl.*  from call_record_logs crl inner join appointment apm on apm.id = crl.apm_id inner join user_account ua on ua.id = crl.doctor_id inner join doctor_profile dp on dp.user_mngment_id = ua.user_mngment_id where dp.hospital_profile_id = "+hospital_id+" and date_part('day',last_update) = date_part('day',current_timestamp) and date_part('month',last_update) = date_part('month',current_timestamp) and date_part('year',last_update) = date_part('year',current_timestamp)  order by last_update desc";
      console.log(query)
      var result = await database.query(query);
      resultJson = result.rows;
      res.send(JSON.stringify({ status: "success", result: resultJson }));
  } catch (err) {
      res.send(JSON.stringify({ status: "error", result: err }));

  }

});

router.get('/getcallmonth', async (req, res, next) => {
  try {
     
      var resultJson = [];
      let hospital_id = 1;
      if(req.query.hospital_id){
        hospital_id = req.query.hospital_id;
      }
      var query = "select count(crl.id),date_part('day',last_update) as timeday,max(last_update)  from call_record_logs crl inner join appointment apm on apm.id = crl.apm_id inner join user_account ua on ua.id = crl.doctor_id inner join doctor_profile dp on dp.user_mngment_id = ua.user_mngment_id where dp.hospital_profile_id = "+hospital_id+" and date_part('month',last_update) = date_part('month',current_timestamp) group by timeday order by timeday asc ";
      console.log(query)
      var result = await database.query(query);
      resultJson = result.rows;
      res.send(JSON.stringify({ status: "success", result: resultJson }));
  } catch (err) {
      res.send(JSON.stringify({ status: "error", result: err }));

  }

});



router.get('/getstripetransaction', async (req, res, next) => {
  try {
     
      var resultJson = [];
      let hospital_id = 1;
      if(req.query.hospital_id){
        hospital_id = req.query.hospital_id;
      }
      var query = "SELECT  count(id),date_part('day',write_date) as timeday,max(write_date) from payment_transaction where payment_gateway='stripe' and date_part('day',write_date) = date_part('day',current_timestamp)  and  date_part('month',write_date) = date_part('month',current_timestamp) and date_part('year',write_date) = date_part('year',current_timestamp) group by timeday order by timeday asc ";
      console.log(query)
      var result = await database.query(query);
      resultJson = result.rows;
      res.send(JSON.stringify({ status: "success", result: resultJson }));
  } catch (err) {
      res.send(JSON.stringify({ status: "error", result: err }));

  }

});


router.get('/getstripetransactiondetail', async (req, res, next) => {
  try {
     
      var resultJson = [];
      let hospital_id = 1;
      if(req.query.hospital_id){
        hospital_id = req.query.hospital_id;
      }
      var query = "SELECT  * from payment_transaction where payment_gateway='stripe' and date_part('day',write_date) = date_part('day',current_timestamp)  and  date_part('month',write_date) = date_part('month',current_timestamp) and date_part('year',write_date) = date_part('year',current_timestamp) order by id desc ";
      console.log(query)
      var result = await database.query(query);
      resultJson = result.rows;
      res.send(JSON.stringify({ status: "success", result: resultJson }));
  } catch (err) {
      res.send(JSON.stringify({ status: "error", result: err }));

  }

});



router.get('/getddtransaction', async (req, res, next) => {
  try {
     
      var resultJson = [];
      let hospital_id = 1;
      if(req.query.hospital_id){
        hospital_id = req.query.hospital_id;
      }
      var query = "SELECT  count(id),date_part('day',write_date) as timeday,max(write_date) from payment_transaction where payment_gateway='drdr' and date_part('day',write_date) = date_part('day',current_timestamp) and  date_part('month',write_date) = date_part('month',current_timestamp) and date_part('year',write_date) = date_part('year',current_timestamp) group by timeday order by timeday asc ";
      console.log(query)
      var result = await database.query(query);
      resultJson = result.rows;
      res.send(JSON.stringify({ status: "success", result: resultJson }));
  } catch (err) {
      res.send(JSON.stringify({ status: "error", result: err }));

  }

});


router.get('/getddtransactiondetail', async (req, res, next) => {
  try {
     
      var resultJson = [];
      let hospital_id = 1;
      if(req.query.hospital_id){
        hospital_id = req.query.hospital_id;
      }
      var query = "SELECT  * from payment_transaction where payment_gateway='drdr' and date_part('day',write_date) = date_part('day',current_timestamp) and  date_part('month',write_date) = date_part('month',current_timestamp) and date_part('year',write_date) = date_part('year',current_timestamp) order by id desc  ";
      console.log(query)
      var result = await database.query(query);
      resultJson = result.rows;
      res.send(JSON.stringify({ status: "success", result: resultJson }));
  } catch (err) {
      res.send(JSON.stringify({ status: "error", result: err }));

  }

});
module.exports = router;
